# README #

Simulations of nanoparticle (NP) internalisation within cells. 

### Overview ###

There are three example simulations. The first assumes that the cells and NPs are contained within a well-mixed environment such that spatial distribution is unimportant. The other two include mesh files that include specific configurations of cells.

* well_mixed  
	This folder contains all the files required to simulate a well-mixed reaction diffusion simulation of NP internalisation into cells. The script can model an arbitrary number of cells which are specified within the main.py file.  
* cuboid  
	This folder contains all files for simulations of NPs within a cuboid region where cells are distributed linearly along the cuboids length. The mesh file is automatically generated for N cells along with the mesh size. This is currently being benchmarked.  
* spheroid  
	This folder contains all files for simulations of NPs within a spheroid region of 55 cells where the cells are randomly distributed. This is a working example but requires the benchmarking to ascertain whether the results are meaningful.  

### Installation ###

* Summary of set up  
The simulations rely heavily on STEPS for the exact solution to the stochastic reaction-diffusion equations. For the simulations that use meshed domains, gmsh is used to create the meshes. All files currently include working .inp files that allow the scripts to be run without gmsh installed. 
* Dependencies  
gmsh can be installed here: http://gmsh.info/  
STEPS can be installed from here: http://steps.sourceforge.net/STEPS/default.php  

* How to run tests  
Benchmarks are run using run.sh files. Currently only the cuboid example includes working benchmark files.


### TODO ###

* Finish benchmarks for well mixed and spheroid
* Compare timings for various mesh fineness and input values
* Parallelisation for improved speed
