
import steps.model as smodel
import steps.geom as sgeom
import steps.utilities.meshio as smeshio
import steps.rng as srng
import steps.solver as solvmod
import math
import numpy as np

import model_3d
import plotting_3d
#import cell_model
#import plotting_fcn
import os

N_cell = 56
file_name = 'sphere_cells'
# gmsh_loc = '/Applications/Gmsh.app/Contents/MacOS/Gmsh'
# mesh_line = "%s ./mesh/%s.geo -setnumber N_cell %s -3 -o ./mesh/%s.inp" % (gmsh_loc, file_name, N_cell, file_name)
# os.system(mesh_line)

# Number of iterations; 
NITER = 1

# The data collection time increment (s) and length scale (m)
DT =  1e-5
DX = 1e-7
t_final = int(60*60*48)    

# The simulation endtime (s)
INT =  0.0501

# Number of molecules injected in centre
NINJECT = 1e3 #need to seriously reduce the number of particles

# The number of receptors per cell
R_NUM = 2e5
# Create the biochemical model
model = model_3d.model(N=N_cell, k_bind = 1e4, r = 5e-9)
#model = cell_model.gen_model(k_diff = DCST, k_bind = 10e2, k_unbind =1e3, k_intern = 10)

# Build mesh
mesh, domain_tets = model_3d.geom(file_name='./mesh/'+file_name,dx = DX, N = N_cell)
#mesh, tets_cell1, tets_cell2, tets_cell3, tets_cell4, tets_vessel = cell_model.gen_geom(dx = DX)

# Create rnadom number generator object
rng = srng.create('mt19937', 512)
rng.initialize(234)

# Create solver object
sim = solvmod.Tetexact(model, mesh, rng)

stable = True
if stable is False:
	sim.reset()
	tpnt = np.arange(0.0, INT, DT)#np.linspace(0.0, t_final, NT)
	ntpnts = tpnt.shape[0]

	# Create the simulation data structures
	ntets = mesh.countTets()
	resX = np.zeros((ntpnts, ntets))

	tetX = mesh.findTetByPoint([0, 0, 0])

	sim.setTetCount(tetX, 'NP', NINJECT)
	sim.setTetClamped(tetX, 'NP', True)

	for n in range(1,N_cell):
		sim.setPatchCount('memb{0}'.format(n), 'R', R_NUM) 

	for i in range(ntpnts):
		sim.run(tpnt[i])
		print(i)
		for n in range(0,N_cell):
			resX[i, n] = sim.getCompCount('cell{0}'.format(n), 'NPi')
	print(resX)
		
else:  
	sim.reset()
	for n in range(N_cell):
		sim.setPatchCount('memb{0}'.format(n), 'R', R_NUM) 

	tetX = mesh.findTetByPoint([0, 0, 0])
	sim.setTetCount(tetX, 'NP', NINJECT)

	plotting_3d.plotres(sim, mesh, INT, DT, N_cell)

#pylab.figure(figsize=(10,7))
#plotting_fcn.plot_binned(100, 50)
