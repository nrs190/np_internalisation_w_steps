import steps.utilities.meshio as smeshio
import steps.utilities.meshctrl as meshctrl
import steps.model as smodel
import steps.geom as sgeom
import steps.rng as srng

import numpy as np
import re
import mmap

def model(T=310.15, k_B=1.3806503e-23, nu=1e-3, r=500e-9, S=10e-6, \
             k_bind=1e9, k_unbind=1e-4,k_intern=1e-5,N_R =1e6, N = 20, DX = 1e-7):
    
    mdl = smodel.Model()
    k_diff = ((k_B*T)/(6*np.pi*nu*r)) 

    print('Diffusion is %g' % k_diff) 
    print('k_B is %g' % (k_unbind/k_bind))

    ############### system species ###############
    # Free NPs
    NP = smodel.Spec('NP', mdl)     
    # internalised NPs
    NPi = smodel.Spec('NPi', mdl)   
    # complexes state: NPs bound to a cell receptor
    NPR = smodel.Spec('NPR', mdl)
    # receptor state: 'naive' state (no bound NPs)
    R = smodel.Spec('R', mdl)

    ############### Reactions and Diffusion ###############
    d = {}
    rxn_ = {}

    # Create volume for diffusion of NPs
    vsys = smodel.Volsys('tissue', mdl)

    for n in range(N):   
        d['memb{0}'.format(n)] = smodel.Surfsys('memb{0}'.format(n), mdl)

        # The 'forward' binding reactions:
        rxn_["bind_{0}".format(n)] = smodel.SReac("bind_{0}".format(n), d['memb{0}'.format(n)], 
            olhs=[NP], slhs = [R], srhs=[NPR], kcst=k_bind)
        # The 'backward' binding reactions:
        rxn_["unbind_{0}".format(n)] = smodel.SReac("unbind_{0}".format(n), d['memb{0}'.format(n)], 
            slhs=[NPR], orhs=[NP], srhs=[R], kcst=k_unbind)
        # The 'internalisation' binding reactions:
        rxn_["intern_{0}".format(n)] = smodel.SReac("intern_{0}".format(n), d['memb{0}'.format(n)], 
            slhs=[NPR], irhs=[NPi], srhs=[R], kcst=k_intern)

    # Diffusion 
    dfsn = smodel.Diff('dfsn', vsys, NP)
    dfsn.setDcst(k_diff)
    return mdl

def geom(file_name = './mesh/tissue_sphere', dx = 1e-7, N = 20):
    print('Number of cells is %i' % N)
    ##TODO: if statement to check for existene of a mesh file to use.
    mesh, nodeproxy, tetproxy, triproxy  = smeshio.importAbaqus(file_name + '.inp', dx)
    smeshio.saveMesh(file_name, mesh)

    tet_groups = tetproxy.blocksToGroups()

    f = open(file_name + '.inp')
    s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)

    vol_tag = []
    while s.find(b'Volume') != -1:
        m = re.search(b'Volume', s)
        if len(vol_tag) < 30:
            volume = s[m.start(): m.end() + 3]
        else:
            volume = s[m.start(): m.end() + 4]
        vol_tag.append(''.join([n for n in str(volume) if n.isdigit()]))
        s = s[m.end():-1]

    domain_tets = tet_groups["Volume186"]# % 186 is specified in the .geo file
    vessel = sgeom.TmComp('tissue', mesh, domain_tets)
    vessel.addVolsys('tissue')
    vol_tag.remove('186')
    print('Successfully found %i cells' %  len(vol_tag))
    msh_ = {}
    memb_ = {}
    cell_ = {}
    for n,tag in enumerate(vol_tag): 
        msh_['c_tets{0}'.format(n)] = tet_groups["Volume%s" % tag]
        msh_['memb_tets{0}'.format(n)] = meshctrl.findOverlapTris(mesh, domain_tets, msh_['c_tets{0}'.format(n)])

        cell_['cell{0}'.format(n)] = sgeom.TmComp('cell{0}'.format(n), mesh, msh_['c_tets{0}'.format(n)])
        cell_['memb{0}'.format(n)] = sgeom.TmPatch('memb{0}'.format(n), mesh, msh_['memb_tets{0}'.format(n)], \
                icomp = cell_['cell{0}'.format(n)], ocomp = vessel)
        cell_['memb{0}'.format(n)].addSurfsys('memb{0}'.format(n))
        
    return mesh, domain_tets

