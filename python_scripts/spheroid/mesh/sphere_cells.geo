// Building a tissue scale region including different cells
// Adapted from t5.geo (example) and sphere.geo

// Parameteres
x0= 0;
y0= 0;
z0 = 0;

r_domain = 10;
r_cell = 1;
lcar1 = 1;
lcar2 = 0.5;
// Build the initial spheroid domain:
Point(1) = {0, 0, 0, lcar1};
Point(2) = {r_domain, 0, 0, lcar1};
Point(3) = {-r_domain, 0, 0, lcar1};
Point(4) = {0, r_domain, 0, lcar1};
Point(5) = {0, -r_domain, 0, lcar1};
Point(6) = {0, 0, r_domain, lcar1};
Point(7) = {0, 0, -r_domain, lcar1};

Circle(1) = {2, 1, 4};
Circle(2) = {4, 1, 3};
Circle(3) = {3, 1, 5};
Circle(4) = {5, 1, 2};
Circle(5) = {2, 1, 6};
Circle(6) = {6, 1, 3};
Circle(7) = {3, 1, 7};
Circle(8) = {7, 1, 2};
Circle(9) = {4, 1, 6};
Circle(10) = {6, 1, 5};
Circle(11) = {5, 1, 7};
Circle(12) = {7, 1, 4};

Line Loop(14) = {2, 7, 12};
Ruled Surface(14) = {14};
Line Loop(16) = {2, -6, -9};
Ruled Surface(16) = {16};
Line Loop(18) = {3, -10, 6};
Ruled Surface(18) = {18};
Line Loop(20) = {3, 11, -7};
Ruled Surface(20) = {20};
Line Loop(22) = {4, -8, -11};
Ruled Surface(22) = {22};
Line Loop(24) = {4, 5, 10};
Ruled Surface(24) = {24};
Line Loop(26) = {1, 9, -5};
Ruled Surface(26) = {26};
Line Loop(28) = {1, -12, 8};
Ruled Surface(28) = {28};
Surface Loop(30) = {14, 16, 18, 20, 22, 24, 26, 28};


// Use a user-defined macro in order to build cells into the domain:

Macro Cell
  // In the following commands we use the reserved variable name `newp', which
  // automatically selects a new point number. This number is chosen as the
  // highest current point number, plus one. (Note that, analogously to `newp',
  // the variables `newl', `news', `newv' and `newreg' select the highest number
  // amongst currently defined curves, surfaces, volumes and `any entities other
  // than points', respectively.)

  p1 = newp; Point(p1) = {x,  y,  z,  lcar2} ;
  p2 = newp; Point(p2) = {x+r_cell,y,  z,  lcar2} ;
  p3 = newp; Point(p3) = {x,  y+r_cell,z,  lcar2} ;
  p4 = newp; Point(p4) = {x,  y,  z+r_cell,lcar2} ;
  p5 = newp; Point(p5) = {x-r_cell,y,  z,  lcar2} ;
  p6 = newp; Point(p6) = {x,  y-r_cell,z,  lcar2} ;
  p7 = newp; Point(p7) = {x,  y,  z-r_cell,lcar2} ;

  c1 = newreg; Circle(c1) = {p2,p1,p7}; c2 = newreg; Circle(c2) = {p7,p1,p5};
  c3 = newreg; Circle(c3) = {p5,p1,p4}; c4 = newreg; Circle(c4) = {p4,p1,p2};
  c5 = newreg; Circle(c5) = {p2,p1,p3}; c6 = newreg; Circle(c6) = {p3,p1,p5};
  c7 = newreg; Circle(c7) = {p5,p1,p6}; c8 = newreg; Circle(c8) = {p6,p1,p2};
  c9 = newreg; Circle(c9) = {p7,p1,p3}; c10 = newreg; Circle(c10) = {p3,p1,p4};
  c11 = newreg; Circle(c11) = {p4,p1,p6}; c12 = newreg; Circle(c12) = {p6,p1,p7};

  // We need non-plane surfaces to define the spherical holes. Here we use ruled
  // surfaces, which can have 3 or 4 sides:

  l1 = newreg; Line Loop(l1) = {c5,c10,c4};    Surface(newreg) = {l1};
  l2 = newreg; Line Loop(l2) = {c9,-c5,c1};    Surface(newreg) = {l2};
  l3 = newreg; Line Loop(l3) = {c12,-c8,-c1};  Surface(newreg) = {l3};
  l4 = newreg; Line Loop(l4) = {c8,-c4,c11};   Surface(newreg) = {l4};
  l5 = newreg; Line Loop(l5) = {-c10,c6,c3};   Surface(newreg) = {l5};
  l6 = newreg; Line Loop(l6) = {-c11,-c3,c7};  Surface(newreg) = {l6};
  l7 = newreg; Line Loop(l7) = {-c2,-c7,-c12}; Surface(newreg) = {l7};
  l8 = newreg; Line Loop(l8) = {-c6,-c9,c2};   Surface(newreg) = {l8};

  // We then store the surface loops identification numbers in a list for later
  // reference (we will need these to define the final volume):

  theloops[t] = newreg ;

  Surface Loop(theloops[t]) = {l8+1,l5+1,l1+1,l2+1,l3+1,l7+1,l6+1,l4+1};

  thecell = newreg ;
  Volume(thecell) = theloops[t] ;

Return

// We can use a `For' loop to generate cells in the domain:
N_cell = 20;
x = x0 + r_domain - 2*r_cell; y = y0; z = r_domain - r_cell; 

For t In {1:56}
  theta = t*Pi;
  x = 2*Sqrt(t)*Sin(t + Pi/5); y = 2*Sqrt(t)*Cos(t+ Pi/5); z = z0; 

  If(t > 20)
    theta = Abs(20 - t);
    x = 2*Sqrt(theta)*Sin(theta+ 2*Pi/5); y = 2*Sqrt(theta)*Cos(theta+ 2*Pi/5); z = z0+3*r_cell; 
    Printf("%g", x);
  EndIf 
  If(t > 20 + 12)
    theta = Abs(20 + 12 - t);
    x = 2*Sqrt(theta)*Sin(theta+ 3*Pi/5); y = 2*Sqrt(theta)*Cos(theta+ 3*Pi/5); z = z0-3*r_cell; 
  EndIf
  If(t > 20 + 12*2)
    theta = Abs(20 +12*2 - t);
    x = 2*Sqrt(theta)*Sin(theta + 4*Pi/5); y = 2*Sqrt(theta)*Cos(theta+ 4*Pi/5); z = z0-6*r_cell; 
  EndIf
  If(t > 20 + 12*2 + 6)
    theta = Abs(20 + 12*2 + 6 - t);
    x = 2*Sqrt(theta)*Sin(theta + 1); y = 2*Sqrt(theta)*Cos(theta + 1); z = z0+6*r_cell; 
  EndIf
  Call Cell ;

  Physical Volume (Sprintf("c%g", t)) = thecell;

  Printf("Cell %g (center = {%g,%g,%g}, radius = %g) has number %g!",
  t, x, y, z, r_cell, thecell) ;
EndFor

theloops[0] = newreg ;

Surface Loop(theloops[0]) = {14, 16, 18, 20, 22, 24, 26, 28} ;

Volume(186) = {theloops[]} ;

Physical Volume ('domain') = 186 ;  //186 is arbitrarily chosen so as not to clash with the for loop