import mmap
import re 
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
def GetTime(sec):
	d = timedelta(seconds = int(sec))

	print("HOURS:MIN:SEC")
	print("%s" % (str(d)))

file_name = 'output2_.txt'

f = open(file_name)
s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)

t = 0
cell_count = [12]
print(len(s))
while s.find(b'Completed in ') != -1:
	m = re.search(b'Completed in ', s)
	print(m.end())
	k = re.search(b' seconds', s)
	time = s[m.end(): k.start()].decode("utf-8") 
	t += float(time)
	m = re.search(b'Number of dead cells is ', s)
	k = re.search(b'\nD', s)
	dead_cells = s[m.end(): k.start()-1].decode("utf-8") 
	cell_count.append(dead_cells)
#    vol_tag.append(''.join([n for n in str(volume) if n.isdigit()]))
	s = s[k.end():-1]
	if len(s) < 199 :
		break

d = timedelta(seconds =int(t))
print('Completed in %s' % str(d))
cell_count = [int(c) for c in cell_count[1:]]
print(len(cell_count))

k_range = [1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9]
k_range = [(1e9*1e-4/k) for k in k_range]
r_range = [1e-6, 1e-7, 1e-8, 1e-9]
output = np.zeros(shape = (len(r_range), len(k_range)))
cell_count.append(1)

i = 0
for m,r in enumerate(r_range):
	for n,k_bind in enumerate(k_range):
		output[m,n] = cell_count[i]
		print(output)
		i += 1
#splt.pcolor(np.log10(k_range), np.log10(r_range), output)

ax = sns.heatmap(output, xticklabels=np.log10(k_range[::-1]), yticklabels=np.log10(r_range), annot=True)
plt.show()