Diffusion is 1e-12 cm^2/s
k_D is 100 nM
Number of cells is 1

Completed in 0.0780189 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 100 nM
Number of cells is 2

Completed in 0.0898731 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 100 nM
Number of cells is 5

Completed in 0.104067 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 100 nM
Number of cells is 10

Completed in 0.134491 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 100 nM
Number of cells is 50

Completed in 0.577044 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 1 nM
Number of cells is 1

Completed in 0.0871551 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 1 nM
Number of cells is 2

Completed in 0.094681 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 1 nM
Number of cells is 5

Completed in 0.107515 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 1 nM
Number of cells is 10

Completed in 0.138292 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 1 nM
Number of cells is 50

Completed in 0.55434 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.01 nM
Number of cells is 1

Completed in 0.0689552 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.01 nM
Number of cells is 2

Completed in 0.091202 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.01 nM
Number of cells is 5

Completed in 0.102342 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.01 nM
Number of cells is 10

Completed in 0.135727 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.01 nM
Number of cells is 50

Completed in 0.555486 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.0001 nM
Number of cells is 1

Completed in 0.0864718 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.0001 nM
Number of cells is 2

Completed in 0.088366 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.0001 nM
Number of cells is 5

Completed in 0.107255 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.0001 nM
Number of cells is 10

Completed in 0.210198 seconds
Number of dead cells is 1 

Diffusion is 1e-12 cm^2/s
k_D is 0.0001 nM
Number of cells is 50

Completed in 0.707433 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 100 nM
Number of cells is 1

Completed in 0.57117 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 100 nM
Number of cells is 2

Completed in 0.529738 seconds
Number of dead cells is 2 

Diffusion is 1e-11 cm^2/s
k_D is 100 nM
Number of cells is 5

Completed in 0.535518 seconds
Number of dead cells is 2 

Diffusion is 1e-11 cm^2/s
k_D is 100 nM
Number of cells is 10

Completed in 0.699198 seconds
Number of dead cells is 2 

Diffusion is 1e-11 cm^2/s
k_D is 100 nM
Number of cells is 50

Completed in 1.24345 seconds
Number of dead cells is 2 

Diffusion is 1e-11 cm^2/s
k_D is 1 nM
Number of cells is 1

Completed in 0.535428 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 1 nM
Number of cells is 2

Completed in 0.569002 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 1 nM
Number of cells is 5

Completed in 0.575367 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 1 nM
Number of cells is 10

Completed in 0.690786 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 1 nM
Number of cells is 50

Completed in 1.14094 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.01 nM
Number of cells is 1

Completed in 0.536313 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.01 nM
Number of cells is 2

Completed in 0.5686 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.01 nM
Number of cells is 5

Completed in 0.572185 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.01 nM
Number of cells is 10

Completed in 0.683784 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.01 nM
Number of cells is 50

Completed in 1.12854 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.0001 nM
Number of cells is 1

Completed in 0.497664 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.0001 nM
Number of cells is 2

Completed in 0.531336 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.0001 nM
Number of cells is 5

Completed in 0.541012 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.0001 nM
Number of cells is 10

Completed in 0.67019 seconds
Number of dead cells is 1 

Diffusion is 1e-11 cm^2/s
k_D is 0.0001 nM
Number of cells is 50

Completed in 1.11633 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 100 nM
Number of cells is 1

Completed in 2.87274 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 100 nM
Number of cells is 2

Completed in 3.57348 seconds
Number of dead cells is 2 

Diffusion is 1e-10 cm^2/s
k_D is 100 nM
Number of cells is 5

Completed in 3.61059 seconds
Number of dead cells is 5 

Diffusion is 1e-10 cm^2/s
k_D is 100 nM
Number of cells is 10

Completed in 4.37991 seconds
Number of dead cells is 5 

Diffusion is 1e-10 cm^2/s
k_D is 100 nM
Number of cells is 50

Completed in 4.94703 seconds
Number of dead cells is 5 

Diffusion is 1e-10 cm^2/s
k_D is 1 nM
Number of cells is 1

Completed in 3.63755 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 1 nM
Number of cells is 2

Completed in 4.16083 seconds
Number of dead cells is 2 

Diffusion is 1e-10 cm^2/s
k_D is 1 nM
Number of cells is 5

Completed in 4.08196 seconds
Number of dead cells is 2 

Diffusion is 1e-10 cm^2/s
k_D is 1 nM
Number of cells is 10

Completed in 5.03508 seconds
Number of dead cells is 2 

Diffusion is 1e-10 cm^2/s
k_D is 1 nM
Number of cells is 50

Completed in 6.85881 seconds
Number of dead cells is 2 

Diffusion is 1e-10 cm^2/s
k_D is 0.01 nM
Number of cells is 1

Completed in 4.11246 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.01 nM
Number of cells is 2

Completed in 4.67866 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.01 nM
Number of cells is 5

Completed in 4.08426 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.01 nM
Number of cells is 10

Completed in 5.42371 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.01 nM
Number of cells is 50

Completed in 5.64889 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.0001 nM
Number of cells is 1

Completed in 3.79078 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.0001 nM
Number of cells is 2

Completed in 4.47827 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.0001 nM
Number of cells is 5

Completed in 4.29782 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.0001 nM
Number of cells is 10

Completed in 4.93135 seconds
Number of dead cells is 1 

Diffusion is 1e-10 cm^2/s
k_D is 0.0001 nM
Number of cells is 50

Completed in 6.28783 seconds
Number of dead cells is 1 

Diffusion is 1e-09 cm^2/s
k_D is 100 nM
Number of cells is 1

Completed in 9.82079 seconds
Number of dead cells is 1 

Diffusion is 1e-09 cm^2/s
k_D is 100 nM
Number of cells is 2

Completed in 21.0283 seconds
Number of dead cells is 2 

Diffusion is 1e-09 cm^2/s
k_D is 100 nM
Number of cells is 5

Completed in 38.3222 seconds
Number of dead cells is 5 

Diffusion is 1e-09 cm^2/s
k_D is 100 nM
Number of cells is 10

Completed in 41.3267 seconds
Number of dead cells is 10 

Diffusion is 1e-09 cm^2/s
k_D is 100 nM
Number of cells is 50

Completed in 55.3409 seconds
Number of dead cells is 15 

Diffusion is 1e-09 cm^2/s
k_D is 1 nM
Number of cells is 1

Completed in 11.7369 seconds
Number of dead cells is 1 

Diffusion is 1e-09 cm^2/s
k_D is 1 nM
Number of cells is 2

Completed in 27.2725 seconds
Number of dead cells is 2 

Diffusion is 1e-09 cm^2/s
k_D is 1 nM
Number of cells is 5

Completed in 30.6205 seconds
Number of dead cells is 5 

Diffusion is 1e-09 cm^2/s
k_D is 1 nM
Number of cells is 10

Completed in 38.1058 seconds
Number of dead cells is 5 

Diffusion is 1e-09 cm^2/s
k_D is 1 nM
Number of cells is 50

Completed in 34.3404 seconds
Number of dead cells is 5 

Diffusion is 1e-09 cm^2/s
k_D is 0.01 nM
Number of cells is 1

Completed in 10.6793 seconds
Number of dead cells is 1 

Diffusion is 1e-09 cm^2/s
k_D is 0.01 nM
Number of cells is 2

Completed in 22.9668 seconds
Number of dead cells is 2 

Diffusion is 1e-09 cm^2/s
k_D is 0.01 nM
Number of cells is 5

Completed in 33.0299 seconds
Number of dead cells is 4 

Diffusion is 1e-09 cm^2/s
k_D is 0.01 nM
Number of cells is 10

Completed in 35.5703 seconds
Number of dead cells is 4 

Diffusion is 1e-09 cm^2/s
k_D is 0.01 nM
Number of cells is 50

Completed in 36.535 seconds
Number of dead cells is 4 

Diffusion is 1e-09 cm^2/s
k_D is 0.0001 nM
Number of cells is 1

Completed in 10.8217 seconds
Number of dead cells is 1 

Diffusion is 1e-09 cm^2/s
k_D is 0.0001 nM
Number of cells is 2

Completed in 23.0706 seconds
Number of dead cells is 2 

Diffusion is 1e-09 cm^2/s
k_D is 0.0001 nM
Number of cells is 5

Completed in 27.4721 seconds
Number of dead cells is 4 

Diffusion is 1e-09 cm^2/s
k_D is 0.0001 nM
Number of cells is 10

Completed in 33.2967 seconds
Number of dead cells is 4 

Diffusion is 1e-09 cm^2/s
k_D is 0.0001 nM
Number of cells is 50

Completed in 36.9017 seconds
Number of dead cells is 4 

Diffusion is 1e-08 cm^2/s
k_D is 100 nM
Number of cells is 1

Completed in 219.822 seconds
Number of dead cells is 1 

Diffusion is 1e-08 cm^2/s
k_D is 100 nM
Number of cells is 2

Completed in 3743.16 seconds
Number of dead cells is 2 

Diffusion is 1e-08 cm^2/s
k_D is 100 nM
Number of cells is 5

Completed in 4515.62 seconds
Number of dead cells is 5 

Diffusion is 1e-08 cm^2/s
k_D is 100 nM
Number of cells is 10

Completed in 711.162 seconds
Number of dead cells is 10 

Diffusion is 1e-08 cm^2/s
k_D is 100 nM
Number of cells is 50

Completed in 777.733 seconds
Number of dead cells is 50 

Diffusion is 1e-08 cm^2/s
k_D is 1 nM
Number of cells is 1

Completed in 47.0864 seconds
Number of dead cells is 1 

Diffusion is 1e-08 cm^2/s
k_D is 1 nM
Number of cells is 2

Completed in 108.996 seconds
Number of dead cells is 2 

Diffusion is 1e-08 cm^2/s
k_D is 1 nM
Number of cells is 5

Completed in 278.445 seconds
Number of dead cells is 5 

Diffusion is 1e-08 cm^2/s
k_D is 1 nM
Number of cells is 10

Completed in 434.073 seconds
Number of dead cells is 10 

Diffusion is 1e-08 cm^2/s
k_D is 1 nM
Number of cells is 50

Completed in 407.57 seconds
Number of dead cells is 16 

Diffusion is 1e-08 cm^2/s
k_D is 0.01 nM
Number of cells is 1

Completed in 50.1421 seconds
Number of dead cells is 1 

Diffusion is 1e-08 cm^2/s
k_D is 0.01 nM
Number of cells is 2

Completed in 112.437 seconds
Number of dead cells is 2 

Diffusion is 1e-08 cm^2/s
k_D is 0.01 nM
Number of cells is 5

Completed in 282.262 seconds
Number of dead cells is 5 

Diffusion is 1e-08 cm^2/s
k_D is 0.01 nM
Number of cells is 10

Completed in 393.3 seconds
Number of dead cells is 10 

Diffusion is 1e-08 cm^2/s
k_D is 0.01 nM
Number of cells is 50

Completed in 435.222 seconds
Number of dead cells is 13 

Diffusion is 1e-08 cm^2/s
k_D is 0.0001 nM
Number of cells is 1

Completed in 54.5877 seconds
Number of dead cells is 1 

Diffusion is 1e-08 cm^2/s
k_D is 0.0001 nM
Number of cells is 2

Completed in 122.533 seconds
Number of dead cells is 2 

Diffusion is 1e-08 cm^2/s
k_D is 0.0001 nM
Number of cells is 5

Completed in 302.948 seconds
Number of dead cells is 5 

Diffusion is 1e-08 cm^2/s
k_D is 0.0001 nM
Number of cells is 10

Completed in 22305.3 seconds
Number of dead cells is 10 

Diffusion is 1e-08 cm^2/s
k_D is 0.0001 nM
Number of cells is 50

Completed in 1990.6 seconds
Number of dead cells is 12 

