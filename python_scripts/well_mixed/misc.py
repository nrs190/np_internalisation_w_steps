import matplotlib.pyplot as plt
from time import sleep

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()


def plot_results(result, tpnt, N_cells = 10):

	print('\n\n Final count in all cells:\n')
	print(result[-1, :])
	plt.figure(figsize=(12,7))
	plt.plot(tpnt, result, linewidth=1., label='NPi (cell_%i)')# % n)

	plt.ylabel('NP/NPi Count')
	plt.xlabel('Time (sec)')
	plt.ylim(0)
	plt.legend()
	plt.show()