import steps.model as smodel
import steps.geom as swm
import steps.rng as srng
import steps.solver as ssolver

import numpy as np
import misc 

def patch_model(T=310.15, k_B=1.3806503e-23, nu=1e-3, D=1e-14, S=10e-6, \
             k_bind=1e9, k_unbind=1e-4,k_intern=1e-5, N_cells=10,N_R =1e6):
    mdl = smodel.Model()

#    k_diff = ((k_B*T)/(6*np.pi*nu*10.0*r)/S/S) #why 10??
#    k_diff = r*1e4
    k_diff = D/S/S
    print('Diffusion is %g cm^2/s' % (1e4*D)) 
    print('k_D is %g nM' % (1e9*k_unbind/k_bind))

    ############### system species ###############
    # Free NPs
    NP = smodel.Spec('NP', mdl)     
    # internalised NPs
    NPi = smodel.Spec('NPi', mdl)   
    # complexes state: NPs bound to a cell receptor
    NPR = smodel.Spec('NPR', mdl)
    # receptor state: 'naive' state (no bound NPs)
    R = smodel.Spec('R', mdl)

    ############### Reactions and Diffusion ###############
    d = {}
    rxn_ = {}
    dfsn_ = {}    

    for n in range(N_cells):    
        d["surfsys{0}".format(n)] = smodel.Surfsys('surfsys%s' % n, mdl)

        # The 'forward' binding reactions:
        rxn_["bind_{0}".format(n)] = smodel.SReac("bind_{0}".format(n), d["surfsys{0}".format(n)], 
            ilhs=[NP], slhs = [R], srhs=[NPR], kcst=k_bind)
        rxn_["unbind_{0}".format(n)] = smodel.SReac("unbind_{0}".format(n), d["surfsys{0}".format(n)], 
            slhs=[NPR], irhs=[NP], srhs=[R], kcst=k_unbind)
        rxn_["intern_{0}".format(n)] = smodel.SReac("intern_{0}".format(n), d["surfsys{0}".format(n)], 
            slhs=[NPR], irhs=[NPi], srhs=[R], kcst=k_intern)

        dfsn_["frwd_{0}".format(n)] = smodel.SReac("frwd_{0}".format(n), d["surfsys{0}".format(n)], 
            ilhs=[NP], orhs=[NP], kcst=k_diff)
        dfsn_["bkwd_{0}".format(n)] = smodel.SReac("bkwd_{0}".format(n), d["surfsys{0}".format(n)], 
            olhs=[NP], irhs=[NP], kcst=k_diff)
    return mdl

def model(T=310.15, k_B=1.3806503e-23, nu=1e-3, r=500e-9, S=10e-6, \
             k_bind=1e9, k_unbind=1e-4,k_intern=1e-5, N_cells=10,N_R =1e6):
    mdl = smodel.Model()

    k_diff = ((k_B*T)/(6*np.pi*nu*10.0*r)/S/S) #why 10??

    print('D =  %g' % (k_diff*1e-4))#for cms #1e-4*1e-7/(S**2)#0.00451389   #units are: s^-1 range is 1e-6 to 1e-9
    print('k_D =  %g' % (1e9*k_unbind/k_bind))

    ############### system species ###############
    # Free NPs
    NP = smodel.Spec('NP', mdl)     
    # internalised NPs
    NPi = smodel.Spec('NPi', mdl)   
    # complexes state: NPs bound to a cell receptor
    NPR = smodel.Spec('NPR', mdl)
    # receptor state: 'naive' state (no bound NPs)
    R = smodel.Spec('R', mdl)

    ############### Reactions and Diffusion ###############
    d = {}
    rxn_ = {}
    dfsn_ = {}    

    for n in range(N_cells):    
        d["surfsys{0}".format(n)] = smodel.Surfsys('surfsys%s' % n, mdl)

        # The 'forward' binding reactions:
        rxn_["bind_{0}".format(n)] = smodel.SReac("bind_{0}".format(n), d["surfsys{0}".format(n)], 
            ilhs=[NP, R], irhs=[NPR], kcst=k_bind)
        rxn_["unbind_{0}".format(n)] = smodel.SReac("unbind_{0}".format(n), d["surfsys{0}".format(n)], 
            ilhs=[NPR], irhs=[NP, R], kcst=k_unbind)
        rxn_["intern_{0}".format(n)] = smodel.SReac("intern_{0}".format(n), d["surfsys{0}".format(n)], 
            ilhs=[NPR], irhs=[NPi, R], kcst=k_intern)

        dfsn_["frwd_{0}".format(n)] = smodel.SReac("frwd_{0}".format(n), d["surfsys{0}".format(n)], 
            ilhs=[NP], orhs=[NP], kcst=k_diff)
        dfsn_["bkwd_{0}".format(n)] = smodel.SReac("bkwd_{0}".format(n), d["surfsys{0}".format(n)], 
            olhs=[NP], irhs=[NP], kcst=k_diff)
    return mdl

def geom(S=10e-6, N_cells = 10):
    ############### create cells ###############
    geom = swm.Geom()
    # Create the vessel compartment
    c_ = {"cell_0" : swm.Comp("cell_0", geom, vol=S**3)}
    m_ = {}

    for n in range(0,N_cells - 1):
        c_["cell_{0}".format(n+1)] = swm.Comp("cell_{0}".format(n+1), geom, vol=S**3)
        m_["memb_{0}".format(n+1)] = swm.Patch("memb_{0}".format(n+1), geom, c_["cell_{0}".format(n+1)], c_["cell_{0}".format(n)])
        m_["memb_{0}".format(n+1)].addSurfsys("surfsys{0}".format(n+1))
        m_["memb_{0}".format(n+1)].setArea(S**2)
    print('Number of cells is %i' % (N_cells - 1))
    return geom

def solver(mdl, geom, N_cells, N_R, t_final, NT, NITER, S=10e-6, opt='comp'):
    r = srng.create('mt19937', 512)
    r.initialize(7233)
    sim = ssolver.Wmdirect(mdl, geom, r)

    tpnt = np.linspace(0.0, t_final, NT)
    res = np.zeros([NT, N_cells])
    resi = np.zeros([NT, N_cells])

#    misc.printProgressBar(0, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)

    for i in range(0, NITER):
        sim.reset()
        sim.setCompConc("cell_0", 'NP', 200e-9)#in moles...
        sim.setCompClamped("cell_0", 'NP', True)
        for n in range(1,N_cells):
            if opt == 'patch': #receptors on outside rather than inside (works better)
                sim.setPatchCount("memb_{0}".format(n), 'R', N_R)
            else:
                sim.setCompConc("cell_{0}".format(n), 'R', (N_R/(6.022e23*S**3)))

        for t in range(0, int(NT/2)):
            sim.run(tpnt[t])
            for n in range(1, N_cells):
                resi[t, n] = sim.getCompCount("cell_{0}".format(n), 'NPi')
#            misc.printProgressBar(t, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)

        sim.setCompClamped("cell_0", 'NP', False)
        for t in range(int(NT/2 + 1), NT):
            sim.run(tpnt[t])
            for n in range(1, N_cells):
                resi[t, n] = sim.getCompCount("cell_{0}".format(n), 'NPi')
 #           misc.printProgressBar(t, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)
    return resi

