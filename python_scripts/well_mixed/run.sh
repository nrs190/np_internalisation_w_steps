#!/bin/bash

D=(  1e-12) #1e-16 1e-15 1e-14 1e-13)
k_a=( 1e3 1e5 1e7 1e9)

N_cells=( 1 2 5 10 50)
i=81
output='output/output.txt'

for d in "${D[@]}"
do
	for k in "${k_a[@]}"
	do
		for N in "${N_cells[@]}"
		do
			./main.py $N $k $d $i | tee -a $output;
			((i++))
		done
	done
done
