from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import mmap
import re 
import seaborn as sns

def GetTime(sec):
	d = timedelta(seconds = int(sec))

	print("HOURS:MIN:SEC")
	print("%s" % (str(d)))

file_name = './output/output.txt'

f = open(file_name)
s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)

t = 0
cell_count = []
timings = []
N_cells = []
N_dead = []
diff_ = []
K_D = []

while s.find(b'Diffusion is ') != -1:
	m = re.search(b'Diffusion is ', s)
	k = re.search(b'cm', s)
	D = s[m.end(): k.start()].decode("utf-8") 
	diff_.append(D)

	m = re.search(b'k_D is ', s)
	k = re.search(b' nM', s)
	K = s[m.end(): k.start()].decode("utf-8") 
	K_D.append(K)

	m = re.search(b'Number of cells is ', s)
	k = re.search(b'\nCompleted', s)
	N = s[m.end(): k.start()-1].decode("utf-8") 
	N_cells.append(N)

	m = re.search(b'Completed in ', s)
	k = re.search(b' seconds', s)
	time = s[m.end(): k.start()].decode("utf-8") 
	timings.append(time)
	t += float(time)

	m = re.search(b'Number of dead cells is ', s)
	k = re.search(b'\nD', s)
	dead_cells = s[m.end(): k.start()-1].decode("utf-8") 
	N_dead.append(dead_cells)

#    vol_tag.append(''.join([n for n in str(volume) if n.isdigit()]))
	s = s[(k.end()-1):]
	if len(s) < 124 :
		break

df = pd.DataFrame({'Diffusion':diff_, 'K_D': K_D, 
						'N_cells':N_cells, 'Time':timings,'N_dead':N_dead})

N_ = [1, 2, 5, 10, 50]
for n,N in enumerate(N_):
	print('\nN_cells = %s\n' % N)
	df_dead = df[['N_cells','Diffusion', 'K_D', 'N_dead']]
	gb = df_dead.groupby(['N_cells'])
	tmp_df = [gb.get_group(x) for x in gb.groups]
	plt_df = tmp_df[n].pivot("Diffusion", "K_D", "N_dead")#.fillna(0)
	print('Number of dead cells:')
	print(plt_df)

	df_time = df[['N_cells','Diffusion', 'K_D', 'Time']]
	gb = df_time.groupby(['N_cells'])
	tmp_df = [gb.get_group(x) for x in gb.groups]
	plt_df = tmp_df[n].pivot("Diffusion", "K_D", "Time")#.fillna(0)
	print('Runtime:')
	print(plt_df)
	#ax = sns.heatmap(plt_df)
	#plt.show()

	#print(df.groupby(['Diffusion', 'K_D', 'N_cells', 'Time']).size())
