#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from time import sleep
import time
import pickle, sys

import wmxd, misc

def main(k_bind, D, N_cells, file_name):
    N_R = 1e6
    t_final = int(60*60*48)
    NT = 60*60
    NITER = 1
    tpnt = np.linspace(0.0, t_final, NT)

    start = time.time()
    wm_mdl = wmxd.patch_model(k_bind=k_bind, D=D, N_cells=N_cells)
    wm_geom = wmxd.geom(N_cells=N_cells)

    kwargs ={'mdl':wm_mdl, 'geom':wm_geom,'N_cells':N_cells,'N_R':N_R,\
                't_final':t_final, 'NT':NT, 'NITER':NITER}
    
    resi = wmxd.solver(**kwargs, opt='patch')
    end = time.time()
    print('\nCompleted in %g seconds' % (end - start))
    #misc.plot_results(result=resi,tpnt=tpnt,N_cells=10)
 
    dead_cells =0 
    for n in range(N_cells):
        if resi[-1,n] > 600:
            dead_cells += 1
   
    print('Number of dead cells is %i \n' % dead_cells)
    # Saving the objects:
    with open('./output/%s.pkl' % file_name, 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([resi], f)
        f.close()

if __name__ == "__main__":
    N_cells = int(sys.argv[1]) + 1
    k_b = float(sys.argv[2])
    D = float(sys.argv[3])
    i = sys.argv[4]
    main(k_bind = k_b, D = D, N_cells = N_cells, file_name = 'run_%s' % i)
    