from __future__ import print_function # for backward compatibility with Py2

import steps.model as smodel
import steps.geom as stetmesh
import steps.utilities.meshio as smeshio
import steps.rng as srng
import steps.solver as solvmod

import pylab
import math

mesh = smeshio.importAbaqus('../mesh/cell_mesh.inp', 1e-7)
smeshio.saveMesh('../mesh/cell_mesh', mesh[0])

def gen_model():
    mdl = smodel.Model()
    A = smodel.Spec('A', mdl)
    ssys = smodel.Surfsys('ssys', mdl) #this is the input of the mesh
    diff_A = smodel.Diff('diffA', ssys, A,  DCST)
    return mdl

def gen_geom(c_x, c_y, c_z):
    mesh = smeshio.loadMesh('mesh/square')[0]

    ntets = mesh.countTets()
    comp = stetmesh.TmComp('cyto', mesh, range(ntets))

    alltris = mesh.getSurfTris()
    # Sort patch triangles as those of positive z
    patch_tris = []
    for t in alltris:
        vert0, vert1, vert2 = mesh.getTri(t)
        if (mesh.getVertex(vert0)[2] > 0.0 and mesh.getVertex(vert1)[2] > 0.0  and mesh.getVertex(vert2)[2] > 0.0 ):
            patch_tris.append(t)

    # Create the patch
    patch = stetmesh.TmPatch('patch', mesh, patch_tris, icomp = comp)
    patch.addSurfsys('ssys')
    patch_tris_n = len(patch_tris)

    trirads = pylab.zeros(patch_tris_n)
    triareas = pylab.zeros(patch_tris_n)

    # Find the central tri
    ctetidx = mesh.findTetByPoint([c_x, c_y, c_z])
    ctet_trineighbs = mesh.getTetTriNeighb(ctetidx)
    ctri_idx=-1
    for t in ctet_trineighbs:
        if t in patch_tris:
            ctri_idx = t

    # Now find the distance of the centre of each tri to the central tri
    cbaryc = mesh.getTriBarycenter(ctri_idx)
    for i in range(patch_tris_n):
        baryc = mesh.getTriBarycenter(patch_tris[i])
        r2 = math.pow((baryc[0]-cbaryc[0]),2) + math.pow((baryc[1]-cbaryc[1]),2) + math.pow((baryc[2]-cbaryc[2]),2)
        r = math.sqrt(r2)
        # Convert to microns and square microns
        trirads[i] = r*1.0e6
        triareas[i] = mesh.getTriArea(patch_tris[i])*1e12

    return mesh, patch_tris, patch_tris_n, ctri_idx, trirads, triareas



mesh = smeshio.loadMesh('mesh/square')[0]
ntets = mesh.countTets()
comp = stetmesh.TmComp('cyto', mesh, range(ntets))

# Number of iterations; plotting dt; sim endtime:
NITER = 100

# The data collection time increment (s)
DT = 1.0

# The simulation endtime (s)
INT = 21.0

# Number of molecules injected in centre
NINJECT = 1000

# The diffusion constant for our diffusing species (m^2/s)
DCST = 0.08e-12

model = gen_model()
tmgeom, patch_tris, patch_tris_n, ctri_idx, trirads, triareas = gen_geom(5e-7,5e-7,1e-7)

#random number generator:
rng = srng.create('mt19937', 512)
rng.initialize(234)

# Create solver object
sim = solvmod.Tetexact(model, tmgeom, rng)

tpnts = pylab.arange(0.0, INT, DT)
ntpnts = tpnts.shape[0]
# Create the  data structure: iterations x time points x tri samples
res = pylab.zeros((NITER, ntpnts, patch_tris_n))


# Run NITER number of iterations:
for j in range(NITER):
    if not j%10: print("Running iteration ", j)
    sim.reset()
    sim.setTriCount(ctri_idx, 'A', NINJECT)
    for i in range(ntpnts):
        sim.run(tpnts[i])
        for k in range(patch_tris_n):
            res[j, i, k] = sim.getTriCount(patch_tris[k], 'A')/triareas[k]

res_mean = pylab.mean(res, axis = 0)
print(res_mean)
def plotres(res_mean, tidx):
    if (tidx >= INT/DT):
        print("Time index is out of range.")
        return
    pylab.scatter(trirads, res_mean[tidx], s=10)
    pylab.xlabel('Radial distance ($\mu$m)')
    pylab.ylabel('Concentration (/$\mu$m$^2$)')
    t = tpnts[tidx]
    pylab.title('Unbounded surface diffusion. Time: ' + str(t) + 's')
    pylab.xlim(0,10)
    pylab.ylim(0)
    pylab.show()

plotres(res_mean,20)