import steps.model as smodel
import steps.geom as swm
import steps.rng as srng
import steps.solver as ssolver

import numpy as np
import matplotlib.pyplot as plt

############### Doesnt work ###############
# A) Receptors need to be definied in molar units if not on a patch
# B) Diffusion cannot link between compartments without a patch



k_diff = 1e-4   #units are: ?
k_bind = 1e2    #units are: Moles
k_unbind = 1e-4 #units are: Moles
k_intern = 1e-5 #units are: Moles

mdl = smodel.Model()
# NP
NP = smodel.Spec('NP', mdl)     #free 
NPi = smodel.Spec('NPi', mdl)   #internalized

############### cell state objects ###############
# receptor state: 'naive' state (no bound NPs)
R1 = smodel.Spec('R1', mdl)
R2 = smodel.Spec('R2', mdl)
R3 = smodel.Spec('R3', mdl)
R4 = smodel.Spec('R4', mdl)

# complexes state: NP bound to one cell 
NPR1 = smodel.Spec('NPR1', mdl)
# receptor state: NP bound to two cells
NPR2 = smodel.Spec('NPR2', mdl)
# receptor state: NP bound to three cells
NPR3 = smodel.Spec('NPR3', mdl)
# receptor state: NP bound to four cells
NPR4 = smodel.Spec('NPR4', mdl)

# Create separate volume systems for compartments A and B
vsys_vessel = smodel.Volsys('vsys_vessel', mdl)
vsys1 = smodel.Volsys('vsys1', mdl)
vsys2 = smodel.Volsys('vsys2', mdl)
vsys3 = smodel.Volsys('vsys3', mdl)
vsys4 = smodel.Volsys('vsys4', mdl)

# The 'forward' binding reactions:
NP_R1_bind_NPR1 = smodel.Reac('NP_R1_bind_NPR1', vsys1,
                            lhs=[NP, R1], rhs=[NPR1], kcst=k_bind)
NP_R2_bind_NPR2 = smodel.Reac('NP_R2_bind_NPR2', vsys2,
                            lhs=[NP, R2], rhs=[NPR2], kcst=k_bind)
NP_R3_bind_NPR3 = smodel.Reac('NP_R3_bind_NPR3', vsys3,
                            lhs=[NP, R3], rhs=[NPR3], kcst=k_bind)
NP_R4_bind_NPR4 = smodel.Reac('NP_R4_bind_NPR4', vsys4,
                            lhs=[NP, R4], rhs=[NPR4], kcst=k_bind)
# Internalisation
NPR1_internal = smodel.Reac('NPR1_internal', vsys1,
                           lhs=[NPR1], rhs=[R1, NPi], kcst=k_intern)
NPR2_internal = smodel.Reac('NPR2_internal', vsys2,
                           lhs=[NPR2], rhs=[R2, NPi], kcst=k_intern)
NPR3_internal = smodel.Reac('NPR3_internal', vsys3,
                           lhs=[NPR3], rhs=[R3, NPi], kcst=k_intern)
NPR4_internal = smodel.Reac('NPR4_internal', vsys4,
                           lhs=[NPR4], rhs=[R4, NPi], kcst=k_intern)

# The 'backward' unbinding reactions:
NPR1_unbind_NP_R1 = smodel.Reac('NPR1_unbind_NP_R', vsys1,
                            lhs=[NPR1], rhs=[R1, NP], kcst=k_unbind)
NPR2_unbind_NP_R2 = smodel.Reac('NPR2_unbind_NP_R', vsys2,
                            lhs=[NPR2], rhs=[R2, NP], kcst=k_unbind)
NPR3_unbind_NP_R3 = smodel.Reac('NPR3_unbind_NP_R', vsys3,
                            lhs=[NPR3], rhs=[R3, NP], kcst=k_unbind)
NPR4_unbind_NP_R4 = smodel.Reac('NPR4_unbind_NP_R', vsys4,
                            lhs=[NPR4], rhs=[R4, NP], kcst=k_unbind)

# Diffusion 
NP_diff_1 = smodel.Reac('NP_diff_0', vsys_vessel, lhs=[NP], rhs=[NP], kcst=k_diff)
NP_diff_1 = smodel.Reac('NP_diff_1', vsys1, lhs=[NP], rhs=[NP], kcst=k_diff)
NP_diff_2 = smodel.Reac('NP_diff_2', vsys2, lhs=[NP], rhs=[NP], kcst=k_diff)
NP_diff_3 = smodel.Reac('NP_diff_3', vsys3, lhs=[NP], rhs=[NP], kcst=k_diff)
NP_diff_4 = smodel.Reac('NP_diff_4', vsys4, lhs=[NP], rhs=[NP], kcst=k_diff)


# Create the vessel compartment
wmgeom = swm.Geom()
vessel = swm.Comp('vessel', wmgeom, vol=(20e-6)**3)
vessel.addVolsys('vsys_vessel')

# Create the cellular compartment
cell_1 = swm.Comp('cell_1', wmgeom, vol=(20e-6)**3)
cell_1.addVolsys('vsys1')
cell_2 = swm.Comp('cell_2', wmgeom, vol=(20e-6)**3)
cell_2.addVolsys('vsys2')
cell_3 = swm.Comp('cell_3', wmgeom, vol=(20e-6)**3)
cell_3.addVolsys('vsys3')
cell_4 = swm.Comp('cell_4', wmgeom, vol=(20e-6)**3)
cell_4.addVolsys('vsys4')

r = srng.create('mt19937', 512)
r.initialize(7233)

sim = ssolver.Wmdirect(mdl, wmgeom, r)
NITER = 5
t_final = int(60*60*48)
NT = 3600
tpnt = np.linspace(0.0, t_final, NT)
res = np.zeros([NITER, NT, 5])
res_std = np.zeros([NT, 5])
res_std1 = np.zeros([NT, 5])
res_std2 = np.zeros([NT, 5])

for i in range (0, NITER):
    sim.reset()
    sim.setCompConc('vessel', 'NP', 200e-9)#in moles...
    sim.setCompClamped('vessel', 'NP', True)
    sim.setCompConc('cell_1', 'R1', 1e6)
    sim.setCompConc('cell_2', 'R2', 1e6)
    sim.setCompConc('cell_3', 'R3', 1e6)
    sim.setCompConc('cell_4', 'R4', 1e6)

    for t in range(0, int(NT/2)):
        sim.run(tpnt[t])
        res[i, t, 0] = sim.getCompCount('cell_1', 'NPi')
        res[i, t, 1] = sim.getCompCount('cell_2', 'NPi')
        res[i, t, 2] = sim.getCompCount('cell_3', 'NPi')
        res[i, t, 3] = sim.getCompCount('cell_4', 'NPi')
        res[i, t, 4] = sim.getCompCount('vessel', 'NP')
    sim.setCompConc('vessel', 'NP', 0)
    for t in range(int(NT/2 + 1), NT):
        sim.run(tpnt[t])
        res[i, t, 0] = sim.getCompCount('cell_1', 'NPi')
        res[i, t, 1] = sim.getCompCount('cell_2', 'NPi')
        res[i, t, 2] = sim.getCompCount('cell_3', 'NPi')
        res[i, t, 3] = sim.getCompCount('cell_4', 'NPi')
        res[i, t, 4] = sim.getCompCount('vessel', 'NP')
    print('finished simulation %g ' % i)
print(res[-1,:])

plt.figure(figsize=(12,7))
res_mean = np.mean(res, 0)
res_std = np.std(res, 0)
res_std1 = res_mean[:,0] + res_std[:,0]
res_std2 = res_mean[:,0] - res_std[:,0]

plt.plot(tpnt, res_mean[:,0], color='teal', linewidth=1., label='NPi (cell_1)')
plt.plot(tpnt, res_mean[:,1], color='blue', linewidth=1.0, label='NPi (cell_2)')
plt.plot(tpnt, res_mean[:,2], color='red', linewidth=1.0, label='NPi (cell_3)')
plt.plot(tpnt, res_mean[:,3], color='green', linewidth=1.0, label='NPi (cell_4)')
plt.plot(tpnt, res_mean[:,4], color='black', linewidth=1., label='NP (vessel)')

plt.xlabel('Time (sec)')
plt.ylim(0)
plt.legend()
plt.show()
print('plot complete')
