import numpy as np
import matplotlib.pyplot as plt
from time import sleep
import time

import wmxd, misc

def main(k_bind, r, N_cells):
    N_R = 1e6
    t_final = int(60*60*48)
    NT = 60*60
    NITER = 1
    tpnt = np.linspace(0.0, t_final, NT)

    start = time.time()
    wm_mdl = wmxd.model(k_bind=k_bind, r=r, N_cells=N_cells)
    wm_geom = wmxd.geom(N_cells=N_cells)

    kwargs ={'mdl':wm_mdl, 'geom':wm_geom,'N_cells':N_cells,'N_R':N_R,'t_final':t_final, 'NT':NT, 'NITER':NITER}
    
    resi = wmxd.solver(**kwargs)
    end = time.time()
    
    print('\nCompleted in %g seconds' % (end - start))
    #misc.plot_results(result=resi,tpnt=tpnt,N_cells=10)
    
    dead_cells =0 
    for n in range(N_cells):
        if resi[-1,n] > 600:
            dead_cells += 1

    print('Number of dead cells is %i ' % dead_cells)

if __name__ == "__main__":
    k_bind = 1e4
    for i in k: 
        for j in r:
            main(k_bind = k_bind, r = r, N_cells = N_cells)
    r = 500e-9
    N_cells = 10 + 1 #for vessel
