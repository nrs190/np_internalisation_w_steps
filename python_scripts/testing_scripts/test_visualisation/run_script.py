# IP3 receptor mesh simulation

import steps.model as smodel
import steps.geom as swm
import steps.rng as srng
import steps.solver as ssolver

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# DIFFUSION

# Source:
#   Allbritton, N.L., Meyer, T., and Stryer, L. (1992).
#   Range of messenger action of calcium ion and inositol
#   1,4,5-triphosphate. Science 258, 1812-1815.
DCST_Ca = 0.065e-9
DCST_IP3 = 0.283e-9

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import ip3r_model

# Import model
mdl = ip3r_model.getModel()

volsys = smodel.Volsys('vsys', mdl)

# Fetch reference to Calcium and IP3 Spec objects
Ca = mdl.getSpec('Ca')
IP3 = mdl.getSpec('IP3')

# Create diffusion rules
Ca_diff = smodel.Diff('Ca_diff', volsys, Ca, DCST_Ca)
IP3_diff = smodel.Diff('IP3_diff', volsys, IP3, DCST_IP3)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Import mesh
import steps.utilities.meshio as meshio

mesh = meshio.loadMesh("ip3r_mesh")[0]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Create random number generator
r = srng.create('mt19937', 512)
r.initialize(456)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Create reaction-diffusion solver object
sim = ssolver.Tetexact(mdl, mesh, r)

# Setup initial condition
sim.setCompConc('cyt', 'Ca', 3.30657e-8)
sim.setCompConc('cyt', 'IP3', 2.5e-6)
sim.setCompConc('ER', 'Ca', 150e-6)
sim.setPatchCount('memb', 'R', 16)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Visualization
import pyqtgraph as pg
import steps.visual as visual

# Visualization initialization
app = pg.mkQApp()

# Create plot display
plots = visual.PlotDisplay("IP3 Receptor Model", size = (600, 400))

