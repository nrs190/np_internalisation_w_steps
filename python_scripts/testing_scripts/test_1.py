import steps.model as smodel
import steps.geom as swm
import steps.rng as srng
import steps.solver as ssolver
import numpy as np
import matplotlib.pyplot as plt

mdl = smodel.Model()

# Calcium
Ca = smodel.Spec('Ca', mdl)
# IP3
IP3 = smodel.Spec('IP3', mdl)
############### receptor state objects ###############
# receptor state: 'naive' state (no bound ligands)
R = smodel.Spec('R', mdl)

# receptor state: bound IP3
RIP3 = smodel.Spec('RIP3', mdl)

# receptor state: bound IP3 and Ca (open)
Ropen = smodel.Spec('Ropen', mdl)
# receptor state: Ca bound to one inactivation site
RCa = smodel.Spec('RCa', mdl)
# receptor state: Ca bound to two inactivation sites
R2Ca = smodel.Spec('R2Ca', mdl)
# receptor state: Ca bound to three inactivation sites
R3Ca = smodel.Spec('R3Ca', mdl)

# receptor state: Ca bound to four inactivation sites
R4Ca = smodel.Spec('R4Ca', mdl)

surfsys = smodel.Surfsys('ssys', mdl)

# The 'forward' binding reactions:
R_bind_IP3_f = smodel.SReac('R_bind_IP3_f', surfsys,
                            olhs=[IP3], slhs=[R], srhs=[RIP3])

RIP3_bind_Ca_f = smodel.SReac('RIP3_bind_Ca_f', surfsys,
                              olhs=[Ca], slhs=[RIP3], srhs=[Ropen])

R_bind_Ca_f = smodel.SReac('R_bind_Ca_f', surfsys,
                           olhs=[Ca], slhs=[R], srhs=[RCa])

RCa_bind_Ca_f = smodel.SReac('RCa_bind_Ca_f', surfsys,
                             olhs=[Ca], slhs=[RCa],srhs=[R2Ca])

R2Ca_bind_Ca_f = smodel.SReac('R2Ca_bind_Ca_f', surfsys,
                              olhs=[Ca], slhs=[R2Ca], srhs=[R3Ca])

R3Ca_bind_Ca_f = smodel.SReac('R3Ca_bind_ca_f', surfsys,
                              olhs=[Ca], slhs=[R3Ca], srhs=[R4Ca])
# The 'backward' unbinding reactions:
R_bind_IP3_b = smodel.SReac('R_bind_IP3_b', surfsys,
                            slhs=[RIP3], orhs=[IP3], srhs=[R])

RIP3_bind_Ca_b = smodel.SReac('RIP3_bind_Ca_b', surfsys,
                              slhs=[Ropen], orhs=[Ca], srhs=[RIP3])

R_bind_Ca_b = smodel.SReac('R_bind_Ca_b', surfsys,
                           slhs=[RCa], orhs=[Ca], srhs=[R])

RCa_bind_Ca_b = smodel.SReac('RCa_bind_Ca_b', surfsys,
                             slhs=[R2Ca], orhs=[Ca], srhs=[RCa])

R2Ca_bind_Ca_b = smodel.SReac('R2Ca_bind_Ca_b', surfsys,
                              slhs=[R3Ca], orhs=[Ca], srhs=[R2Ca])

R3Ca_bind_Ca_b = smodel.SReac('R3Ca_bind_ca_b', surfsys,
                              slhs=[R4Ca], orhs=[Ca], srhs=[R3Ca])

# Ca ions passing through open IP3R channel
R_Ca_channel_f = smodel.SReac('R_Ca_channel_f', surfsys,\
                              ilhs=[Ca], slhs=[Ropen], orhs=[Ca], srhs=[Ropen])

R_bind_IP3_f.setKcst(1000e6)
R_bind_IP3_b.setKcst(25800)
RIP3_bind_Ca_f.setKcst(8000e6)
RIP3_bind_Ca_b.setKcst(2000)
R_bind_Ca_f.setKcst(8.889e6)
R_bind_Ca_b.setKcst(5)
RCa_bind_Ca_f.setKcst(20e6)
RCa_bind_Ca_b.setKcst(10)
R2Ca_bind_Ca_f.setKcst(40e6)
R2Ca_bind_Ca_b.setKcst(15)
R3Ca_bind_Ca_f.setKcst(60e6)
R3Ca_bind_Ca_b.setKcst(20)
R_Ca_channel_f.setKcst(4e9)

wmgeom = swm.Geom()

# Create the cytosol compartment
cyt = swm.Comp('cyt', wmgeom)
cyt.setVol(1.6572e-19)

# Create the Endoplasmic Reticulum compartment
ER = swm.Comp('ER', wmgeom, vol=1.968e-20)

# ER is the 'inner' compartment, cyt is the 'outer' compartment
memb = swm.Patch('memb', wmgeom, ER, cyt)
memb.addSurfsys('ssys')
memb.setArea(0.4143e-12)

r = srng.create('mt19937', 512)
r.initialize(7233)

sim = ssolver.Wmdirect(mdl, wmgeom, r)
NITER = 100
tpnt = np.arange(0.0, 0.201, 0.001)
res = np.zeros([NITER, 201, 2])
res_std = np.zeros([201, 2])
res_std1 = np.zeros([201, 2])
res_std2 = np.zeros([201, 2])

for i in range (0, NITER):
    sim.reset()
    sim.setCompConc('cyt', 'Ca', 3.30657e-8)
    sim.setCompCount('cyt', 'IP3', 6)
    sim.setCompConc('ER', 'Ca', 150e-6)
    sim.setCompClamped('ER', 'Ca', True)
    sim.setPatchCount('memb', 'R', 160)
    for t in range(0, 201):
        sim.run(tpnt[t])
        res[i, t, 0] = sim.getCompConc('ER', 'Ca')
        res[i, t, 1] = sim.getCompConc('cyt', 'Ca')
print(res[-1,:])
