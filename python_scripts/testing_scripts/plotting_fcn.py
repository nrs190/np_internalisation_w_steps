import pyqtgraph as pg
import steps.visual as visual
import pylab

def plot_binned(t_idx, bin_n = 100, solver='unknown'):
  if (t_idx > tpnts.size):
    print("Time index is out of range.")
    return

  # Create structure to record z-position of tetrahedron
  z_tets = np.zeros(ntets)

  zbound_min = mesh.getBoundMin()[1]

  # Now find the distance of the centre of the tets to the Z lower face
  for i in range(ntets):
    baryc = mesh.getTetBarycenter(i)
    z = baryc[1] - zbound_min
    # Convert to microns and save
    z_tets[i] = z*1.0e6

  # Find the maximum and minimum z of all tetrahedrons
  z_max = z_tets.max()
  z_min = z_tets.min()

  # Set up the bin structures, recording the individual bin volumes
  z_seg = (z_max-z_min)/bin_n
  bin_mins = np.zeros(bin_n+1)
  z_tets_binned = np.zeros(bin_n)
  bin_vols = np.zeros(bin_n)

  # Now sort the counts into bins for species 'X'
  z = z_min
  for b in range(bin_n + 1):
    bin_mins[b] = z
    if (b!=bin_n): z_tets_binned[b] = z + z_seg/2.0
    z+=z_seg
  bin_counts = [None]*bin_n
  for i in range(bin_n): bin_counts[i] = []
  for i in range((resX[t_idx].size)):
    i_z = z_tets[i]
    for b in range(bin_n):
      if(i_z>=bin_mins[b] and i_z<bin_mins[b+1]):
        bin_counts[b].append(resX[t_idx][i])
        bin_vols[b]+=sim.getTetVol(i)
        break

  # Convert to concentration in arbitrary units
  bin_concs = np.zeros(bin_n)
  for c in range(bin_n):
    for d in range(bin_counts[c].__len__()):
      bin_concs[c] += bin_counts[c][d]
    bin_concs[c]/=(bin_vols[c]*1.0e18)

  t = tpnts[t_idx]

  # Plot the data
  pylab.scatter(z_tets_binned, bin_concs, label = 'X', color = 'blue')

  # Repeat the process for species 'Y'- separate from 'X' for clarity:
  z = z_min
  for b in range(bin_n + 1):
    bin_mins[b] = z
    if (b!=bin_n): z_tets_binned[b] = z + z_seg/2.0
    z+=z_seg
  bin_counts = [None]*bin_n
  for i in range(bin_n): bin_counts[i] = []
  for i in range((resY[t_idx].size)):
    i_z = z_tets[i]
    for b in range(bin_n):
      if(i_z>=bin_mins[b] and i_z<bin_mins[b+1]):
        bin_counts[b].append(resY[t_idx][i])
        break
  bin_concs = np.zeros(bin_n)
  for c in range(bin_n):
    for d in range(bin_counts[c].__len__()):
      bin_concs[c] += bin_counts[c][d]
    bin_concs[c]/=(bin_vols[c]*1.0e18)

  pylab.scatter(z_tets_binned, bin_concs, label = 'Y', color = 'red')

  pylab.xlabel('Z axis (microns)', fontsize=16)
  pylab.ylabel('Bin concentration (N/m^3)', fontsize=16)
  pylab.ylim(0)
  pylab.xlim(0, 2)
  pylab.legend(numpoints=1)
  pylab.title('Simulation with '+ solver)
  pylab.show()

def plotres():
  # Visualization initialization
  app = pg.mkQApp()

  # Create plot display
  plots = visual.PlotDisplay("Simple model", size = (600, 400))

  # Create Plots
  pen = pg.mkPen(color=(255,255,255), width=2)
  p = plots.addCompSpecPlot("<span style='font-size: 16pt'>patchA_X", sim, "compA", "Y", data_size = 1000, measure = "conc", pen=(255, 0.647 * 255, 0))
  p.getAxis('left').setPen(pen)
  p.getAxis('bottom').setPen(pen)
  p.showGrid(x=True, y=True)
  labelStyle = {'color': '#ffffff', 'font-size': '16px'}
  p.setLabel('bottom', 'Time', 's', **labelStyle)

  plots.nextRow()

  p = plots.addCompSpecPlot("<span style='font-size: 16pt'>patchB_X", sim, "compB", "Y", data_size = 1000, measure = "conc", pen=(255, 0.647 * 255, 0))
  p.getAxis('left').setPen(pen)
  p.getAxis('bottom').setPen(pen)
  p.showGrid(x=True, y=True)
  p.setLabel('bottom', 'Time', 's', **labelStyle)

  # Create simulation displays < only consider full view
  #ER_display = visual.SimDisplay("ER", w = 600, h = 400)
  full_display = visual.SimDisplay("Full View", w = 600, h = 400)

  # Create static mesh components
  compA_view = visual.VisualCompMesh("compA", full_display, mesh, "compA", color = [0.678, 1.000, 0.184, 0.05])
  compB_view = visual.VisualCompMesh("compA", full_display, mesh, "compB", color = [0.941, 1.000, 0.941, 0.05])
# memb_view = visual.VisualPatchMesh("memb", full_display, mesh, "memb", color = [1.000, 0.973, 0.863, 0.05])

  # Create dynamic species components
  X_compA = visual.VisualCompSpec("X_compA", full_display, mesh, sim, "compA", "X", [1.000, 0.647, 0.000, 1.0], spec_size = 0.005)
  X_compB = visual.VisualCompSpec("X_compB", full_display, mesh, sim, "compB", "X", [1.000, 0.000, 0.000, 1.0], spec_size = 0.005)
  Y_compA = visual.VisualCompSpec("Y_compA", full_display, mesh, sim, "compA", "Y", [1.000, 0.647, 0.000, 1.0], spec_size = 0.005)
  Y_compB = visual.VisualCompSpec("Y_compB", full_display, mesh, sim, "compB", "Y", [1.000, 0.647, 1.000, 1.0], spec_size = 0.005)
  #Y_compB = visual.VisualCompSpec("Y_compB", full_display, mesh, sim, "Y", {"R" : [0.0, 0.0, 1.0, 1.0], "RIP3" : [1.0, 0.0, 1.0, 0.2], "Ropen" : [1.0, 0.0, 1.0, 1.0], "RCa" : [0.0, 0.0, 1.0, 0.8], "R2Ca" : [0.0, 0.0, 1.0, 0.6], "R3Ca" : [0.0, 0.0, 1.0, 0.4], "R4Ca" : [0.0, 0.0, 1.0, 0.2]}, spec_size = 0.01)

  # Add associated components to individual displays - not used
  #ER_display.addItem(ER_view)
  #ER_display.addItem(Ca_ER)

  # Add simulation and displays to control
  x = visual.SimControl([sim], [full_display],[plots], end_time= 0.1001, upd_interval =  0.001)

  # Enter visualization loop
  app.exec_()