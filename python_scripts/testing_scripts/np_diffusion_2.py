import steps.model as smodel
import steps.geom as swm
import steps.rng as srng
import steps.solver as ssolver

import numpy as np
import matplotlib.pyplot as plt
mdl = smodel.Model()
#from time import sleep
import time
start = time. time()
# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

############### cell state objects ###############
# Free NP
NP = smodel.Spec('NP', mdl)     
# internalised NP
NPi = smodel.Spec('NPi', mdl)   
# complexes state: NP bound to a cell receptor
NPR = smodel.Spec('NPR', mdl)
# receptor state: 'naive' state (no bound NPs)
R = smodel.Spec('R', mdl)

surfsys0 = smodel.Surfsys('ssys0', mdl)
surfsys1 = smodel.Surfsys('ssys1', mdl)
surfsys2 = smodel.Surfsys('ssys2', mdl)
surfsys3 = smodel.Surfsys('ssys3', mdl)
surfsys4 = smodel.Surfsys('ssys4', mdl)

T = 310.15
k_B = 1.3806503e-23
nu = 1e-3
r = 500e-9 #to 5e-9 to 500e-9
S = 10e-6 #largest cell dimension 10um
k_diff = ((k_B*T)/(6*np.pi*nu*10.0*r)/S/S) #why 10??
print('Diffusion is %g' % k_diff)
#1e-4*1e-7/(S**2)#0.00451389   #units are: s^-1 range is 1e-6 to 1e-9
k_bind = 1e9 #units are: (Ms)^-1 #range is 1e3 to 1e9
k_unbind = 1e-4 #units are: s^-1
k_intern = 1e-5 #units are: Moles 

RNum = 1e3

# The 'forward' binding reactions:
NP_R1_bind_NPR1 = smodel.SReac('NP_R1_bind_NPR1', surfsys1,
                            ilhs=[NP], slhs = [R], srhs=[NPR], kcst=k_bind)
NP_R2_bind_NPR2 = smodel.SReac('NP_R2_bind_NPR2', surfsys2,
                            ilhs=[NP], slhs = [R], srhs=[NPR], kcst=k_bind)
NP_R3_bind_NPR3 = smodel.SReac('NP_R3_bind_NPR3', surfsys3,
                            ilhs=[NP], slhs = [R], srhs=[NPR], kcst=k_bind)
NP_R4_bind_NPR4 = smodel.SReac('NP_R4_bind_NPR4', surfsys4,
                            ilhs=[NP], slhs = [R], srhs=[NPR], kcst=k_bind)
# Internalisation
NPR1_internal = smodel.SReac('NPR1_internal', surfsys1,
                           slhs=[NPR], irhs=[NPi], srhs=[R], kcst=k_intern)
NPR2_internal = smodel.SReac('NPR2_internal', surfsys2,
                           slhs=[NPR], irhs=[NPi], srhs=[R], kcst=k_intern)
NPR3_internal = smodel.SReac('NPR3_internal', surfsys3,
                           slhs=[NPR], irhs=[NPi], srhs=[R], kcst=k_intern)
NPR4_internal = smodel.SReac('NPR4_internal', surfsys4,
                           slhs=[NPR], irhs=[NPi], srhs=[R], kcst=k_intern)

# The 'backward' unbinding reactions:
NPR1_unbind_NP_R1 = smodel.SReac('NPR1_unbind_NP_R', surfsys1,
                            slhs=[NPR], irhs=[NP], srhs=[R], kcst=k_unbind)
NPR2_unbind_NP_R2 = smodel.SReac('NPR2_unbind_NP_R', surfsys2,
                            slhs=[NPR], irhs=[NP], srhs=[R], kcst=k_unbind)
NPR3_unbind_NP_R3 = smodel.SReac('NPR3_unbind_NP_R', surfsys3,
                            slhs=[NPR], irhs=[NP], srhs=[R], kcst=k_unbind)
NPR4_unbind_NP_R4 = smodel.SReac('NPR4_unbind_NP_R', surfsys4,
                            slhs=[NPR], irhs=[NP], srhs=[R], kcst=k_unbind)
# Diffusion
diff01 = smodel.SReac('diff01', surfsys1, ilhs=[NP], orhs=[NP], kcst=k_diff)
diff10 = smodel.SReac('diff10', surfsys1, olhs=[NP], irhs=[NP], kcst=k_diff)

diff12 = smodel.SReac('diff12', surfsys2, ilhs=[NP], orhs=[NP], kcst=k_diff)
diff21 = smodel.SReac('diff21', surfsys2, olhs=[NP], irhs=[NP], kcst=k_diff)

diff23 = smodel.SReac('diff23', surfsys3, ilhs=[NP], orhs=[NP], kcst=k_diff)
diff32 = smodel.SReac('diff32', surfsys3, olhs=[NP], irhs=[NP], kcst=k_diff)

diff34 = smodel.SReac('diff34', surfsys4, ilhs=[NP], orhs=[NP], kcst=k_diff)
diff43 = smodel.SReac('diff43', surfsys4, olhs=[NP], irhs=[NP], kcst=k_diff)

# Create the vessel compartment
wmgeom = swm.Geom()
vessel = swm.Comp('vessel', wmgeom, vol= S**3)

# Create the cellular compartment
cell_1 = swm.Comp('cell_1', wmgeom, vol=S**3)
cell_2 = swm.Comp('cell_2', wmgeom, vol=S**3)
cell_3 = swm.Comp('cell_3', wmgeom, vol=S**3)
cell_4 = swm.Comp('cell_4', wmgeom, vol=S**3)

# vessel is the 'inner' compartment, cell is the 'outer' compartment
memb_1 = swm.Patch('memb_1', wmgeom, cell_1, vessel)
memb_1.addSurfsys('ssys1')
memb_1.setArea(S**2)

memb_2 = swm.Patch('memb_2', wmgeom, cell_2, cell_1)
memb_2.addSurfsys('ssys2')
memb_2.setArea(S**2)

memb_3 = swm.Patch('memb_3', wmgeom, cell_3, cell_2)
memb_3.addSurfsys('ssys3')
memb_3.setArea(S**2)

memb_4 = swm.Patch('memb_4', wmgeom, cell_4, cell_3)
memb_4.addSurfsys('ssys4')
memb_4.setArea(S**2)

r = srng.create('mt19937', 512)
r.initialize(7233)

sim = ssolver.Wmdirect(mdl, wmgeom, r)
NITER = 1
t_final = int(60*60*48)
NT = 60*60*48 #number of timesteps
tpnt = np.linspace(0.0, t_final, NT)
res = np.zeros([NT, 5])
resi = np.zeros([NT, 5])

res_std = np.zeros([NT, 5])
res_std1 = np.zeros([NT, 5])
res_std2 = np.zeros([NT, 5])
printProgressBar(0, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)

for i in range(0, NITER):
    sim.reset()
    sim.setCompConc('vessel', 'NP', 200e-9)#in moles...
    sim.setCompClamped('vessel', 'NP', True)
    sim.setPatchCount('memb_1', 'R', RNum)
    sim.setPatchCount('memb_2', 'R', RNum)
    sim.setPatchCount('memb_3', 'R', RNum)
    sim.setPatchCount('memb_4', 'R', RNum)

    for t in range(0, int(NT/2)):
        sim.run(tpnt[t])
        res[t, 0] = sim.getCompCount('cell_1', 'NP')
        res[t, 1] = sim.getCompCount('cell_2', 'NP')
        res[t, 2] = sim.getCompCount('cell_3', 'NP')
        res[t, 3] = sim.getCompCount('cell_4', 'NP')
        res[t, 4] = sim.getCompCount('vessel', 'NP')

        resi[t, 0] = sim.getCompCount('cell_1', 'NPi')
        resi[t, 1] = sim.getCompCount('cell_2', 'NPi')
        resi[t, 2] = sim.getCompCount('cell_3', 'NPi')
        resi[t, 3] = sim.getCompCount('cell_4', 'NPi')
        #resi[t, 4] = sim.getCompCount('vessel', 'NPi')
        printProgressBar(t, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)

    sim.setCompClamped("vessel", 'NP', False)
    for t in range(int(NT/2 + 1), NT):
        sim.run(tpnt[t])
        res[t, 0] = sim.getCompCount('cell_1', 'NP')
        res[t, 1] = sim.getCompCount('cell_2', 'NP')
        res[t, 2] = sim.getCompCount('cell_3', 'NP')
        res[t, 3] = sim.getCompCount('cell_4', 'NP')
        res[t, 4] = sim.getCompCount('vessel', 'NP')

        resi[t, 0] = sim.getCompCount('cell_1', 'NPi')
        resi[t, 1] = sim.getCompCount('cell_2', 'NPi')
        resi[t, 2] = sim.getCompCount('cell_3', 'NPi')
        resi[t, 3] = sim.getCompCount('cell_4', 'NPi')
        #resi[t, 4] = sim.getCompCount('vessel', 'NPi')
        printProgressBar(t, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)
    end = time. time()
    print('\nCompleted in %g seconds' % (end - start))
print(res)
plt.figure(figsize=(12,7))
#res_mean = np.mean(res, 0)
#res_std = np.std(res, 0)
#res_std1 = res_mean[:,0] + res_std[:,0]
#res_std2 = res_mean[:,0] - res_std[:,0]

plt.plot(tpnt, res[:,0], color='teal', linewidth=1., label='NP (cell_1)')
plt.plot(tpnt, res[:,1], color='blue', linewidth=1.0, label='NP (cell_2)')
plt.plot(tpnt, res[:,2], color='red',  linewidth=1.0, label='NP (cell_3)')
plt.plot(tpnt, res[:,3], color='green', linewidth=1.0, label='NP (cell_4)')
#plt.plot(tpnt, res[:,4], color='black', linewidth=1., label='NP (vessel)')

plt.plot(tpnt, resi[:,0], color='teal', linewidth=2., label='NPi (cell_1)')
plt.plot(tpnt, resi[:,1], color='blue', linewidth=2.0, label='NPi (cell_2)')
plt.plot(tpnt, resi[:,2], color='red',  linewidth=2.0, label='NPi (cell_3)')
plt.plot(tpnt, resi[:,3], color='green', linewidth=2.0, label='NPi (cell_4)')
#plt.plot(tpnt, resi[:,4], color='black', linewidth=2., label='NPi (vessel)')

plt.xlabel('Time (sec)')
plt.ylim(0)
plt.legend()
plt.show()

