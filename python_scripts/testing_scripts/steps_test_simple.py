import matplotlib.pyplot as plt
import steps.geom as swm
import numpy as np
import steps.model as smodel
import steps.solver as ssolver
import steps.rng as srng

mdl = smodel.Model()
vsys = smodel.Volsys('vsys', mdl)
wmgeom = swm.Geom()
# Create the vessel compartment
comp = swm.Comp('comp', wmgeom, vol=20e-6**3)
comp.addVolsys('vsys')

# Create the cell compartment
#cell1 = swm.Comp('cell1', wmgeom, vol=20e-6**3)

### Species
# Nanoparticle (assume 1)
NP = smodel.Spec('NP', mdl)
# NPi - internatilised nanoparticles
NPi = smodel.Spec('NPi', mdl)

# receptor state: 'naive' state (no bound ligands)
R = smodel.Spec('R', mdl)

# NP-C state: Nanoparticle-cell complexes
C = smodel.Spec('C', mdl)

### Reactants
# The 'forward' binding reactions:
NP_bind_C_f = smodel.Reac('NP_bind_C_f', vsys,
                            lhs=[NP, R], rhs=[C])

# The 'backward' unbinding reactions:
NPi_bind_C_b = smodel.Reac('NP_bind_C_b', vsys,
                            lhs=[C], rhs=[NP, R])
# The 'forward' internalisation reaction:
C_bind_NPi_f = smodel.Reac('C_bind_NPi_f', vsys,
                            lhs=[C], rhs=[NPi, R])

NP_bind_C_f.kcst = 0.4
NPi_bind_C_b.kcst = 0.4
C_bind_NPi_f.kcst = 0.2

NITER = 1
tpnt = np.arange(0.0, 0.201, 0.001)
res = np.zeros([NITER, 201, 2])
res_std = np.zeros([201, 2])
res_std1 = np.zeros([201, 2])
res_std2 = np.zeros([201, 2])
plt.figure(figsize=(12,7))

r = srng.create('mt19937', 256)
r.initialize(23412)
sim = ssolver.Wmdirect(mdl, wmgeom, r)

for i in range (0, NITER):
    sim.reset()
    sim.setCompConc('comp', 'NP', 1e-4)
    sim.setCompConc('comp', 'C', 31.4e-6)
    sim.setCompConc('comp', 'R', 31.4e-6)
    sim.setCompConc('comp', 'NPi', 31.4e-6)
    #sim.setCompClamped('ve', 'Ca', True)
    #sim.setPatchCount('memb', 'R', 160)
    for t in range(0, 201):
        sim.run(tpnt[t])
        res[i, t, 0] = sim.getCompConc('comp', 'NP')
        res[i, t, 1] = sim.getCompConc('comp', 'NPi')
    plt.plot(tpnt, res[i,:,0], color='blue', linewidth=0.1)
    plt.show()
