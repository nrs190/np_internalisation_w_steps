import steps.model as smodel
import steps.geom as swm
import steps.rng as srng
import steps.solver as ssolver

import numpy as np
import matplotlib.pyplot as plt
mdl = smodel.Model()

#      k_a,k_d  k_i
# NP + R <--> C --> NPi + R

# NP
NP = smodel.Spec('NP', mdl)     #free 
NPi = smodel.Spec('NPi', mdl)   #internalized

############### cell state objects ###############
# receptor state: 'naive' state (no bound NPs)
R = smodel.Spec('R', mdl)

# complexes state: NP bound to one cell 
NPR1 = smodel.Spec('NPR1', mdl)
# receptor state: NP bound to two cells
NPR2 = smodel.Spec('NPR2', mdl)
# receptor state: NP bound to three cells
NPR3 = smodel.Spec('NPR3', mdl)
# receptor state: NP bound to four cells
NPR4 = smodel.Spec('NPR4', mdl)

surfsys = smodel.Surfsys('ssys', mdl)

# The 'forward' binding reactions:
NP_R_bind_NPR1 = smodel.SReac('NP_R_bind_NPR1', surfsys,
                            olhs=[NP], slhs=[R], srhs=[NPR1])
NPR1_internal = smodel.SReac('NPR1_internal', surfsys,
                           slhs=[NPR1], irhs=[NPi])
NPR2_internal = smodel.SReac('NPR2_internal', surfsys,
                           slhs=[NPR2], irhs=[NPi])
NPR3_internal = smodel.SReac('NPR3_internal', surfsys,
                           slhs=[NPR3], irhs=[NPi])
NPR4_internal = smodel.SReac('NPR4_internal', surfsys,
                           slhs=[NPR4], irhs=[NPi])

# Ca ions passing through open IP3R channel                 <- May be better to model diffusion like this
NP_diff = smodel.SReac('R_Ca_channel_f', surfsys,\
                             ilhs=[NP], slhs=[R], orhs=[NP], srhs=[R])

#NPR1_diff_NPR2 = smodel.SReac('NPR1_diff_NPR2', surfsys,
   #                           slhs=[NPR1], srhs=[NPR2])
#NPR2_diff_NPR3 = smodel.SReac('NPR2_diff_NPR2', surfsys,
 #                             slhs=[NPR2], srhs=[NPR3])
#NPR3_diff_NPR4 = smodel.SReac('NPR3_diff_NPR4', surfsys,
 #                             slhs=[NPR3], srhs=[NPR4])

# The 'backward' unbinding reactions:
NPR1_unbind_NP_R = smodel.SReac('NPR1_unbind_NP_R', surfsys,
                            slhs=[NPR1], olhs=[NP], srhs=[R])
NPR2_diff_NPR1 = smodel.SReac('NPR2_diff_NPR1', surfsys,
                              slhs=[NPR2], srhs=[NPR1])
NPR3_diff_NPR2 = smodel.SReac('NPR3_diff_NPR2', surfsys,
                              slhs=[NPR3], srhs=[NPR2])
NPR4_diff_NPR3 = smodel.SReac('NPR4_diff_NPR3', surfsys,
                              slhs=[NPR4], srhs=[NPR3])
k_diff = 1e-14   #units are: ?
k_bind = 1e2    #units are: Moles
k_unbind = 1e-4 #units are: Moles
k_intern = 1e-5 #units are: Moles

NP_R_bind_NPR1.setKcst(k_bind)
NPR1_unbind_NP_R.setKcst(k_unbind)
NPR1_internal.setKcst(k_intern)
NPR2_internal.setKcst(k_intern)
NPR3_internal.setKcst(k_intern)
NP_diff.setKcst(k_diff)
#NPR1_diff_NPR2.setKcst(k_diff)
#NPR2_diff_NPR3.setKcst(k_diff)
#NPR3_diff_NPR4.setKcst(k_diff)
#NPR2_diff_NPR1.setKcst(k_diff)
#NPR3_diff_NPR2.setKcst(k_diff)
#NPR4_diff_NPR3.setKcst(k_diff)

wmgeom = swm.Geom()

# Create the vessel compartment
vessel = swm.Comp('vessel', wmgeom)
vessel.setVol(20e-6**3)

# Create the cellular compartment
cells = swm.Comp('cells', wmgeom, vol=(4*20e-6)**3)

# cells is the 'inner' compartment, vessel is the 'outer' compartment
memb = swm.Patch('memb', wmgeom, cells, vessel)
memb.addSurfsys('ssys')
memb.setArea(20e-6**2)

r = srng.create('mt19937', 512)
r.initialize(7233)

sim = ssolver.Wmdirect(mdl, wmgeom, r)
NITER = 5
t_final = int(60*60*48)
NT = 3600
tpnt = np.linspace(0.0, t_final, NT)
res = np.zeros([NITER, NT, 4])
res_std = np.zeros([NT, 4])
res_std1 = np.zeros([NT, 4])
res_std2 = np.zeros([NT, 4])

for i in range (0, NITER):
    sim.reset()
    sim.setCompConc('vessel', 'NP',200e-9) #in moles...
    sim.setPatchCount('memb', 'R', 1e6*4)
    print(i)
    for t in range(0, int(NT/2)):
        sim.run(tpnt[t])
        res[i, t, 0] = sim.getCompCount('cells', 'NPi')
        res[i, t, 1] = sim.getPatchCount('memb', 'NPR2')
        res[i, t, 2] = sim.getPatchCount('memb', 'NPR3')
        res[i, t, 3] = sim.getPatchCount('memb', 'NPR4')
    sim.setCompConc('vessel', 'NP', 0)
    for t in range(int(NT/2 + 1), NT):
        sim.run(tpnt[t])
        res[i, t, 0] = sim.getPatchCount('memb', 'NPR1')
        res[i, t, 1] = sim.getPatchCount('memb', 'NPR2')
        res[i, t, 2] = sim.getPatchCount('memb', 'NPR3')
        res[i, t, 3] = sim.getPatchCount('memb', 'NPR4')
    print('finished simulation')
print(res[-1,:])

plt.figure(figsize=(12,7))
res_mean = np.mean(res, 0)
res_std = np.std(res, 0)
res_std1 = res_mean[:,0] + res_std[:,0]
res_std2 = res_mean[:,0] - res_std[:,0]

plt.plot(tpnt, res_mean[:,0], color='black', linewidth=2.0, label='Internalised NPs')
plt.plot(tpnt, res_mean[:,1], color='blue', linewidth=2.0, label='NPR2')
plt.plot(tpnt, res_mean[:,2], color='red', linewidth=2.0, label='NPR3')
plt.plot(tpnt, res_mean[:,3], color='green', linewidth=2.0, label='NPR4')

plt.xlabel('Time (sec)')
plt.ylim(0)
plt.legend()
plt.show()
print('plot complete')


