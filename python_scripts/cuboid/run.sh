#!/bin/bash

gmsh_loc=/Applications/Gmsh.app/Contents/MacOS/Gmsh
file=cuboid_mesh

D=( 1e-16 1e-14 1e-13) #D=( 1e-16 1e-15 1e-14 1e-13)
k_a=( 1e3 1e5 1e7 1e9)

lc1=(  1. 5.) #lc1=( .5 1. 5.)
lc2=( .5 1. 5.)
N_cells=( 1 2 5 10 50)
i=400
output='output/output.txt'

for d in "${D[@]}"
do
	for k in "${k_a[@]}"
	do
		for l1 in "${lc1[@]}"
		do
			echo 'lc1 is' $l1 | tee -a $output
			for l2 in "${lc2[@]}"
			do
				echo 'lc2 is' $l2 | tee -a $output
				for N in "${N_cells[@]}"
				do
					$gmsh_loc ./mesh/$file.geo \
						-setnumber N_cell $N\
						-setnumber lcar1 $l1\
						-setnumber lcar2 $l2\
						 -3 -o ./mesh/$file.inp -v 0;
					./main.py $N $k $d $i | tee -a $output;
					((i++))
				done
			done
		done
	done
done
