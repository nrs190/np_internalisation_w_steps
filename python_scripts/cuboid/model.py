import steps.utilities.meshio as smeshio
import steps.utilities.meshctrl as meshctrl
import steps.solver as solvmod
import steps.model as smodel
import steps.geom as sgeom
import steps.rng as srng

import numpy as np
import mmap
import re

import misc, plotting

def model(T=310.15, k_B=1.3806503e-23, nu=1e-3, D=500e-9, S=10e-6, \
             k_bind=1e9, k_unbind=1e-4,k_intern=1e-5,N_R =1e6, N = 20, DX = 5e-6):
    
    mdl = smodel.Model()
    k_diff = D#((k_B*T)/(6*10*np.pi*nu*D))

    print('Diffusion is %g cm^2/s' % (1e4*k_diff)) 
    print('k_D is %g nM' % (1e9*k_unbind/k_bind))

    ############### system species ###############
    # Free NPs
    NP = smodel.Spec('NP', mdl)     
    # internalised NPs
    NPi = smodel.Spec('NPi', mdl)   
    # complexes state: NPs bound to a cell receptor
    NPR = smodel.Spec('NPR', mdl)
    # receptor state: 'naive' state (no bound NPs)
    R = smodel.Spec('R', mdl)

    ############### Reactions and Diffusion ###############
    d = {}
    rxn_ = {}

    # Create volume for diffusion of NPs
    vsys1 = smodel.Volsys('tissue', mdl)
    vsys2 = smodel.Volsys('vessel', mdl)

    for n in range(N):   
        d['memb{0}'.format(n)] = smodel.Surfsys('memb{0}'.format(n), mdl)

        # The 'forward' binding reactions:
        rxn_["bind_{0}".format(n)] = smodel.SReac("bind_{0}".format(n), d['memb{0}'.format(n)], 
            olhs=[NP], slhs = [R], srhs=[NPR], kcst=k_bind)
        # The 'backward' binding reactions:
        rxn_["unbind_{0}".format(n)] = smodel.SReac("unbind_{0}".format(n), d['memb{0}'.format(n)], 
            slhs=[NPR], orhs=[NP], srhs=[R], kcst=k_unbind)
        # The 'internalisation' binding reactions:
        rxn_["intern_{0}".format(n)] = smodel.SReac("intern_{0}".format(n), d['memb{0}'.format(n)], 
            slhs=[NPR], irhs=[NPi], srhs=[R], kcst=k_intern)

    # Diffusion 
    dfsn1 = smodel.Diff('dfsn1', vsys1, NP, dcst = k_diff)
    dfsn2 = smodel.Diff('dfsn2', vsys2, NP, dcst = k_diff)
    return mdl

def geom(file_name = './mesh/cuboid_mesh', dx = 5e-6, N = 20):
    ##TODO: if statement to check for existence of a mesh file to use.
    mesh, nodeproxy, tetproxy, triproxy  = smeshio.importAbaqus(file_name + '.inp', dx)
    smeshio.saveMesh(file_name, mesh)

    tet_groups = tetproxy.blocksToGroups()

    f = open(file_name + '.inp')
    s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)

    vol_tag = []
    while s.find(b'Volume') != -1:
        m = re.search(b'Volume', s)
        if len(vol_tag) < 30:
            volume = s[m.start(): m.end() + 3]
        else:
            volume = s[m.start(): m.end() + 4]
        vol_tag.append(''.join([n for n in str(volume) if n.isdigit()]))
        s = s[m.end():-1]
    
    domain_tets = tet_groups["Volume186"]# % 186 is specified in the .geo file
    tissue_tets = []
    vessel_tets = []
    vessel_tris = set()
    tissue_tris = set()
    for t in domain_tets:
        barycz = mesh.getTetBarycenter(t)[2]
        tris = mesh.getTetTriNeighb(t)
        if barycz < 2.5*dx:
            vessel_tets.append(t)
            vessel_tris.add(tris[0])
            vessel_tris.add(tris[1])
            vessel_tris.add(tris[2])
            vessel_tris.add(tris[3])
        else:
            tissue_tets.append(t)
            tissue_tris.add(tris[0])
            tissue_tris.add(tris[1])
            tissue_tris.add(tris[2])
            tissue_tris.add(tris[3])          
    
    tissue = sgeom.TmComp('tissue', mesh, tissue_tets)
    tissue.addVolsys('tissue')
    vessel = sgeom.TmComp('vessel', mesh, vessel_tets)
    vessel.addVolsys('vessel')
    tissue_vessel_db = tissue_tris.intersection(vessel_tris)
    diffb = sgeom.DiffBoundary('vessel_dfsn', mesh, tissue_vessel_db)

    vol_tag.remove('186')
    print('Number of cells is %i' % len(vol_tag))
    msh_ = {}
    memb_ = {}
    cell_ = {}
    for n,tag in enumerate(vol_tag): 
        msh_['c_tets{0}'.format(n)] = tet_groups["Volume%s" % tag]
        msh_['memb_tets{0}'.format(n)] = meshctrl.findOverlapTris(mesh, domain_tets, msh_['c_tets{0}'.format(n)])

        cell_['cell{0}'.format(n)] = sgeom.TmComp('cell{0}'.format(n), mesh, msh_['c_tets{0}'.format(n)])
        cell_['memb{0}'.format(n)] = sgeom.TmPatch('memb{0}'.format(n), mesh, msh_['memb_tets{0}'.format(n)], \
                icomp = cell_['cell{0}'.format(n)], ocomp = vessel)
        cell_['memb{0}'.format(n)].addSurfsys('memb{0}'.format(n))
        
    return mesh, domain_tets

def solver(mdl, mesh, N_cells, N_R, t_final, DT, NINJECT, S=10e-6, visualise = False):

    # Create rnadom number generator object
    rng = srng.create('mt19937', 512)
    rng.initialize(234)

    # Create solver object
    sim = solvmod.Tetexact(mdl, mesh, rng)

    sim.reset()
    tpnt = np.linspace(0.0, t_final, DT)
    ntpnts = tpnt.shape[0]

    # Create the simulation data structures
    ntets = mesh.countTets()
    resX = np.zeros((ntpnts, N_cells))

    tetX = mesh.findTetByPoint([0, 0, 0])

#    sim.setTetCount(tetX, 'NP', NINJECT)
#    sim.setTetClamped(tetX, 'NP', True)

    sim.setCompConc('vessel', 'NP', 200e-9) # slower...
    sim.setCompClamped('vessel', 'NP', True)
    
    for n in range(N_cells):
        sim.setPatchCount('memb{0}'.format(n), 'R', N_R) 

    sim.setDiffBoundaryDiffusionActive('vessel_dfsn', 'NP', True)
    if visualise == True:
        #need to reduce time for this to be effective 
        plotting.plotres(sim, mesh, t_final= t_final, DT=DT, N_cells=N_cells)
#        plotting.plotres(sim, mesh, t_final= 0.0501, DT=1e-5, N_cells=N_cells)
        return 'Simulation complete'

    else:
        # misc.printProgressBar(0, ntpnts, prefix = 'Progress:', suffix = 'Complete', length = 40)

        for t in range(0, int(DT)):
            if t > int(DT/2):
                sim.setCompClamped("vessel", 'NP', False)
            # misc.printProgressBar(t, ntpnts, prefix = 'Progress:', suffix = 'Complete', length = 40)
            sim.run(tpnt[t])
            for n in range(N_cells):
                resX[t, n] = sim.getCompCount('cell{0}'.format(n), 'NPi')
        return resX    