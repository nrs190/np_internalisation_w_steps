#!/usr/bin/env python

import pickle
import time, os, sys

import model

def main(k_bind, D, N_cells = 20, file_name = 'cuboid_mesh', visualise=False):

    # gmsh_loc = '/Applications/Gmsh.app/Contents/MacOS/Gmsh'
    # mesh_line = "%s ./mesh/%s.geo -setnumber N_cell %s -3 -o ./mesh/%s.inp -v 0" % (gmsh_loc, 'cuboid_mesh', N_cells, 'cuboid_mesh')
    # os.system(mesh_line)

    # Number of iterations; 
    NITER = 1

    # The data collection time increment (s) and length scale (m)
    DT =  60*60
    DX = 5e-6 #used for scaling the mesh units

    # The simulation endtime (s)
    t_final = int(60*60*48)#

    # Number of molecules injected in centre
    NINJECT = 1e5 #

    # The number of receptors per cell
    N_R = 1e6

    mdl = model.model(N=N_cells, k_bind = k_b, D = D)

    # Build mesh
    mesh, domain_tets = model.geom(dx = DX, N = N_cells)

    start = time.time()

    kwargs ={'mdl':mdl, 'mesh':mesh,'N_cells':N_cells,'N_R':N_R,\
                't_final':t_final, 'DT':DT, 'NINJECT':NINJECT, 'visualise':visualise}

    resX = model.solver(**kwargs)

    end = time.time()
    print('\nCompleted in %g seconds' % (end - start))      
    if visualise is False:
        with open('./output/%s.pkl' % file_name, 'wb') as f:  
                pickle.dump(resX, f)
                f.close()

        dead_cells =0 
        for n in range(N_cells):
            if resX[-1,n] > 600:
                dead_cells += 1
        print('Number of dead cells is %i \n' % dead_cells)
        # print(resX)

    return '\n\n'
if __name__ == "__main__":
   # k_range = [1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9]
   # r_range = [1e-6, 1e-7, 1e-8, 1e-9]
    N_cells = int(sys.argv[1])
    k_b = float(sys.argv[2])
    D = float(sys.argv[3])
    i = sys.argv[4]
    v = False
    main(k_bind = k_b, D = D, N_cells = N_cells, file_name = 'run_%s' % i, visualise=v)

