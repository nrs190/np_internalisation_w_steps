// Building a tissue scale region including different cells
// Adapted from t5.geo (example)

//lcar1 = 1.;
//lcar2 = .5;

// Centre coordinates
x0 = 0.0; y0 = 0.0; z0 = 0.0;

// Number of cells
// N_cell = 50;

// Domain dimensions (L_x,L_y)
a = 1.1; b = 1.1; 

// Cell radius
r = 0.91*a; //to avoid intersections

// Domain length (L_z)
c = (N_cell+1)*3*r;

Point(6) = {x0 - a, y0 - b, z0, lcar1};      Point(8) = {x0 - a, y0 + b, z0, lcar1};
Point(9) = {x0 + a, y0 + b, z0, lcar1};      Point(10) = {x0 - a, y0 - b, z0 + c, lcar1};
Point(11) = {x0 - a, y0 + b, z0 + c, lcar1}; Point(12) = {x0 + a, y0 + b, z0 + c, lcar1};
Point(13) = {x0 + a, y0 - b, z0 + c, lcar1}; Point(14) = {x0 + a, y0 - b, z0, lcar1};

Line(1) = {8,9};    Line(2) = {9,12}; 
Line(3) = {12,11};  Line(4) = {11,8};   
Line(5) = {9,14};   Line(6) = {14,13}; 
Line(7) = {13,12};  Line(8) = {11,10}; 
Line(9) = {10,13};  Line(10) = {10,6};  
Line(11) = {6,8};  Line(12) = {6,14};

Line Loop(26) = {11,1,5,-12};   Plane Surface(27) = {26};
Line Loop(28) = {-4,-1,-2,-3};  Plane Surface(29) = {28};
Line Loop(30) = {-7,2,-5,-6};   Plane Surface(31) = {30};
Line Loop(32) = {6,-9,10,12};   Plane Surface(33) = {32};
Line Loop(34) = {7,3,8,9};      Plane Surface(35) = {34};
Line Loop(36) = {-10,-11,4,-8}; Plane Surface(37) = {36};

// Use a user-defined macro in order to build cells into the domain:

Macro Cell

  // In the following commands we use the reserved variable name `newp', which
  // automatically selects a new point number. This number is chosen as the
  // highest current point number, plus one. (Note that, analogously to `newp',
  // the variables `newl', `news', `newv' and `newreg' select the highest number
  // amongst currently defined curves, surfaces, volumes and `any entities other
  // than points', respectively.)

  p1 = newp; Point(p1) = {x,  y,  z,  lcar2} ;
  p2 = newp; Point(p2) = {x+r,y,  z,  lcar2} ;
  p3 = newp; Point(p3) = {x,  y+r,z,  lcar2} ;
  p4 = newp; Point(p4) = {x,  y,  z+r,lcar2} ;
  p5 = newp; Point(p5) = {x-r,y,  z,  lcar2} ;
  p6 = newp; Point(p6) = {x,  y-r,z,  lcar2} ;
  p7 = newp; Point(p7) = {x,  y,  z-r,lcar2} ;

  c1 = newreg; Circle(c1) = {p2,p1,p7}; c2 = newreg; Circle(c2) = {p7,p1,p5};
  c3 = newreg; Circle(c3) = {p5,p1,p4}; c4 = newreg; Circle(c4) = {p4,p1,p2};
  c5 = newreg; Circle(c5) = {p2,p1,p3}; c6 = newreg; Circle(c6) = {p3,p1,p5};
  c7 = newreg; Circle(c7) = {p5,p1,p6}; c8 = newreg; Circle(c8) = {p6,p1,p2};
  c9 = newreg; Circle(c9) = {p7,p1,p3}; c10 = newreg; Circle(c10) = {p3,p1,p4};
  c11 = newreg; Circle(c11) = {p4,p1,p6}; c12 = newreg; Circle(c12) = {p6,p1,p7};

  // We need non-plane surfaces to define the spherical holes. Here we use ruled
  // surfaces, which can have 3 or 4 sides:

  l1 = newreg; Line Loop(l1) = {c5,c10,c4};    Surface(newreg) = {l1};
  l2 = newreg; Line Loop(l2) = {c9,-c5,c1};    Surface(newreg) = {l2};
  l3 = newreg; Line Loop(l3) = {c12,-c8,-c1};  Surface(newreg) = {l3};
  l4 = newreg; Line Loop(l4) = {c8,-c4,c11};   Surface(newreg) = {l4};
  l5 = newreg; Line Loop(l5) = {-c10,c6,c3};   Surface(newreg) = {l5};
  l6 = newreg; Line Loop(l6) = {-c11,-c3,c7};  Surface(newreg) = {l6};
  l7 = newreg; Line Loop(l7) = {-c2,-c7,-c12}; Surface(newreg) = {l7};
  l8 = newreg; Line Loop(l8) = {-c6,-c9,c2};   Surface(newreg) = {l8};

  // We then store the surface loops identification numbers in a list for later
  // reference (we will need these to define the final volume):

  theloops[t] = newreg ;

  Surface Loop(theloops[t]) = {l8+1,l5+1,l1+1,l2+1,l3+1,l7+1,l6+1,l4+1};

  thecell = newreg ;
  Volume(thecell) = theloops[t] ;

Return

// We can use a `For' loop to generate cells in the domain:

x = x0; y = y0; z = z0 + r; 

For t In {1:N_cell}

  z += 3*r ;

  // 'Cell' macro:

  Call Cell ;

  Physical Volume (Sprintf("c%g", t)) = thecell;

  Printf("Cell %g (center = {%g,%g,%g}, radius = %g) has number %g!",
  t, x, y, z, r, thecell) ;

EndFor

theloops[0] = newreg ;

Surface Loop(theloops[0]) = {35,31,29,37,33,27} ;

Volume(186) = {theloops[]} ;

Physical Volume ('domain') = 186 ;  //186 is arbitrarily chosen so as not to clash with the for loop