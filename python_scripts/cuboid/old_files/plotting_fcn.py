import pyqtgraph as pg
import steps.visual as visual
import pylab

def plot_binned(t_idx, bin_n = 100, solver='unknown'):
  if (t_idx > tpnts.size):
    print("Time index is out of range.")
    return

  # Create structure to record z-position of tetrahedron
  z_tets = np.zeros(ntets)

  zbound_min = mesh.getBoundMin()[1]

  # Now find the distance of the centre of the tets to the Z lower face
  for i in range(ntets):
    baryc = mesh.getTetBarycenter(i)
    z = baryc[1] - zbound_min
    # Convert to microns and save
    z_tets[i] = z*1.0e6

  # Find the maximum and minimum z of all tetrahedrons
  z_max = z_tets.max()
  z_min = z_tets.min()

  # Set up the bin structures, recording the individual bin volumes
  z_seg = (z_max-z_min)/bin_n
  bin_mins = np.zeros(bin_n+1)
  z_tets_binned = np.zeros(bin_n)
  bin_vols = np.zeros(bin_n)

  # Now sort the counts into bins for species 'X'
  z = z_min
  for b in range(bin_n + 1):
    bin_mins[b] = z
    if (b!=bin_n): z_tets_binned[b] = z + z_seg/2.0
    z+=z_seg
  bin_counts = [None]*bin_n
  for i in range(bin_n): bin_counts[i] = []
  for i in range((resX[t_idx].size)):
    i_z = z_tets[i]
    for b in range(bin_n):
      if(i_z>=bin_mins[b] and i_z<bin_mins[b+1]):
        bin_counts[b].append(resX[t_idx][i])
        bin_vols[b]+=sim.getTetVol(i)
        break

  # Convert to concentration in arbitrary units
  bin_concs = np.zeros(bin_n)
  for c in range(bin_n):
    for d in range(bin_counts[c].__len__()):
      bin_concs[c] += bin_counts[c][d]
    bin_concs[c]/=(bin_vols[c]*1.0e18)

  t = tpnts[t_idx]

  # Plot the data
  pylab.scatter(z_tets_binned, bin_concs, label = 'X', color = 'blue')

  # Repeat the process for species 'Y'- separate from 'X' for clarity:
  z = z_min
  for b in range(bin_n + 1):
    bin_mins[b] = z
    if (b!=bin_n): z_tets_binned[b] = z + z_seg/2.0
    z+=z_seg
  bin_counts = [None]*bin_n
  for i in range(bin_n): bin_counts[i] = []
  for i in range((resY[t_idx].size)):
    i_z = z_tets[i]
    for b in range(bin_n):
      if(i_z>=bin_mins[b] and i_z<bin_mins[b+1]):
        bin_counts[b].append(resY[t_idx][i])
        break
  bin_concs = np.zeros(bin_n)
  for c in range(bin_n):
    for d in range(bin_counts[c].__len__()):
      bin_concs[c] += bin_counts[c][d]
    bin_concs[c]/=(bin_vols[c]*1.0e18)

  pylab.scatter(z_tets_binned, bin_concs, label = 'Y', color = 'red')

  pylab.xlabel('Z axis (microns)', fontsize=16)
  pylab.ylabel('Bin concentration (N/m^3)', fontsize=16)
  pylab.ylim(0)
  pylab.xlim(0, 2)
  pylab.legend(numpoints=1)
  pylab.title('Simulation with '+ solver)
  pylab.show()

def plotres(sim,mesh, INT, DT):
  # Visualization initialization
  app = pg.mkQApp()

  # Create plot display
  plots = visual.PlotDisplay("Simple model", size = (600, 400))

  # Create Plots
  pen = pg.mkPen(color=(255,255,255), width=2)
  p = plots.addCompSpecPlot("<span style='font-size: 16pt'>cell1_NP", sim, "cell1", "NP", data_size = 1000, measure = "conc", pen=(255, 0.647 * 255, 0))
  p.getAxis('left').setPen(pen)
  p.getAxis('bottom').setPen(pen)
  p.showGrid(x=True, y=True)
  labelStyle = {'color': '#ffffff', 'font-size': '16px'}
  p.setLabel('bottom', 'Time', 's', **labelStyle)

  plots.nextRow()

  p = plots.addCompSpecPlot("<span style='font-size: 16pt'>vessel_NP", sim, "vessel", "NP", data_size = 1000, measure = "conc", pen=(255, 0.647 * 255, 0))
  p.getAxis('left').setPen(pen)
  p.getAxis('bottom').setPen(pen)
  p.showGrid(x=True, y=True)
  p.setLabel('bottom', 'Time', 's', **labelStyle)


  plots.nextRow()

  p = plots.addCompSpecPlot("<span style='font-size: 16pt'>cell1_NPi", sim, "cell1", "NPi", data_size = 1000, measure = "conc", pen=(255, 0.647 * 255, 0))
  p.getAxis('left').setPen(pen)
  p.getAxis('bottom').setPen(pen)
  p.showGrid(x=True, y=True)
  p.setLabel('bottom', 'Time', 's', **labelStyle)

  # Create simulation displays < only consider full view
  #ER_display = visual.SimDisplay("ER", w = 600, h = 400)
  full_display = visual.SimDisplay("Full View", w = 600, h = 400)

  # Create static mesh components
  vessel_view = visual.VisualCompMesh("vessel", full_display, mesh, "vessel", color = [0.500, 0.000, 0.000, 0.2])
  cell1_view = visual.VisualCompMesh("cell1", full_display, mesh, "cell1", color = [0.0, 0.700, 0.7, 0.1])
  cell2_view = visual.VisualCompMesh("cell2", full_display, mesh, "cell2", color = [0.0, 0.700, 0.5, 0.1])
  cell3_view = visual.VisualCompMesh("cell3", full_display, mesh, "cell3", color = [0.0, 0.300, 0.7, 0.1])
  cell4_view = visual.VisualCompMesh("cell4", full_display, mesh, "cell4", color = [0.0, 0.300, 0.5, 0.1])

  # Create dynamic species components
  NP_vesel = visual.VisualCompSpec("NP_vesel", full_display, mesh, sim, "vessel", "NP",[1.000, 0.000, 1.000, 1.0], spec_size = 0.005)
  NP_cell1 = visual.VisualCompSpec("NP_cell1", full_display, mesh, sim, "cell1", "NP", [1.000, 0.647, 0.000, 1.0], spec_size = 0.005)
  NP_cell2 = visual.VisualCompSpec("NP_cell2", full_display, mesh, sim, "cell2", "NP", [1.000, 0.000, 0.000, 1.0], spec_size = 0.005)
  NP_cell3 = visual.VisualCompSpec("NP_cell3", full_display, mesh, sim, "cell3", "NP", [1.000, 0.647, 0.000, 1.0], spec_size = 0.005)
  NP_cell4 = visual.VisualCompSpec("NP_cell4", full_display, mesh, sim, "cell4", "NP", [1.000, 0.647, 1.000, 1.0], spec_size = 0.005)

  NPi_vesel = visual.VisualCompSpec("NP_vesel", full_display, mesh, sim, "vessel", "NPi",[0.500, 0.000, 0.000, 1.0], spec_size = 0.05)
  NPi_cell1 = visual.VisualCompSpec("NP_cell1", full_display, mesh, sim, "cell1", "NPi", [0.500, 0.000, 0.000, 1.0], spec_size = 0.05)
  NPi_cell2 = visual.VisualCompSpec("NP_cell2", full_display, mesh, sim, "cell2", "NPi", [1.500, 0.000, 0.000, 1.0], spec_size = 0.05)
  NPi_cell3 = visual.VisualCompSpec("NP_cell3", full_display, mesh, sim, "cell3", "NPi", [0.500, 0.000, 0.000, 1.0], spec_size = 0.05)
  NPi_cell4 = visual.VisualCompSpec("NP_cell4", full_display, mesh, sim, "cell4", "NPi", [0.500, 0.000, 0.000, 1.0], spec_size = 0.05)
  # Add associated components to individual displays - not used
  #ER_display.addItem(ER_view)
  #ER_display.addItem(Ca_ER)

  # Add simulation and displays to control
  x = visual.SimControl([sim], [full_display],[plots], end_time= INT, upd_interval =  DT)

  # Enter visualization loop
  app.exec_()