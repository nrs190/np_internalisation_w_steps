import steps.utilities.meshio as smeshio
import steps.utilities.meshctrl as meshctrl
import steps.model as smodel
import steps.geom as sgeom
import steps.rng as srng


def get_model(k_diff = 7e-9/(10e-6**2), k_bind = 1e2, k_unbind = 1e-4, k_intern = 1e-5):
    
    mdl = smodel.Model()
    # NP
    NP = smodel.Spec('NP', mdl)     #free 
    NPi = smodel.Spec('NPi', mdl)   #internalized

    ############### cell state objects ###############
    # receptor state: 'naive' state (no bound NPs)
    R1 = smodel.Spec('R1', mdl)
    R2 = smodel.Spec('R2', mdl)
    R3 = smodel.Spec('R3', mdl)
    R4 = smodel.Spec('R4', mdl)

    # complexes state: NP bound to one cell 
    NPR1 = smodel.Spec('NPR1', mdl)
    # receptor state: NP bound to two cells
    NPR2 = smodel.Spec('NPR2', mdl)
    # receptor state: NP bound to three cells
    NPR3 = smodel.Spec('NPR3', mdl)
    # receptor state: NP bound to four cells
    NPR4 = smodel.Spec('NPR4', mdl)

    # Create volume for diffusion of NPs
    vsys = smodel.Volsys('tissue', mdl)
    ssys1 = smodel.Surfsys('memb1', mdl)
    ssys2 = smodel.Surfsys('memb2', mdl)
    ssys3 = smodel.Surfsys('memb3', mdl)
    ssys4 = smodel.Surfsys('memb4', mdl)


    # The 'forward' binding reactions:
    NP_R1_bind_NPR1 = smodel.SReac('NP_R1_bind_NPR1', ssys1,
                                olhs=[NP], slhs = [R1], srhs=[NPR1], kcst=k_bind)
    NP_R2_bind_NPR2 = smodel.SReac('NP_R2_bind_NPR2', ssys2,
                                olhs=[NP], slhs = [R2], srhs=[NPR2], kcst=k_bind)
    NP_R3_bind_NPR3 = smodel.SReac('NP_R3_bind_NPR3', ssys3,
                                olhs=[NP], slhs = [R3], srhs=[NPR3], kcst=k_bind)
    NP_R4_bind_NPR4 = smodel.SReac('NP_R4_bind_NPR4', ssys4,
                                olhs=[NP], slhs = [R4], srhs=[NPR4], kcst=k_bind)
    # Internalisation
    NPR1_internal = smodel.SReac('NPR1_internal', ssys1,
                               slhs=[NPR1], srhs=[R1], irhs =[NPi], kcst=k_intern)
    NPR2_internal = smodel.SReac('NPR2_internal', ssys2,
                               slhs=[NPR2], srhs=[R2], irhs =[NPi], kcst=k_intern)
    NPR3_internal = smodel.SReac('NPR3_internal', ssys3,
                               slhs=[NPR3], srhs=[R3], irhs =[NPi], kcst=k_intern)
    NPR4_internal = smodel.SReac('NPR4_internal', ssys4,
                               slhs=[NPR4], srhs=[R4], irhs =[NPi], kcst=k_intern)

    # The 'backward' unbinding reactions:
    NPR1_unbind_NP_R1 = smodel.SReac('NPR1_unbind_NP_R', ssys1,
                                slhs=[NPR1], srhs=[R1], orhs = [NP], kcst=k_unbind)
    NPR2_unbind_NP_R2 = smodel.SReac('NPR2_unbind_NP_R', ssys2,
                                slhs=[NPR2], srhs=[R2], orhs = [NP], kcst=k_unbind)
    NPR3_unbind_NP_R3 = smodel.SReac('NPR3_unbind_NP_R', ssys3,
                                slhs=[NPR3], srhs=[R3], orhs = [NP], kcst=k_unbind)
    NPR4_unbind_NP_R4 = smodel.SReac('NPR4_unbind_NP_R', ssys4,
                                slhs=[NPR4], srhs=[R4], orhs = [NP], kcst=k_unbind)

    # Diffusion 
    diff_A = smodel.Diff('NP_diff', vsys, NP)
    diff_A.setDcst(k_diff)
    return mdl

def gen_geom(file_name = './mesh/tissue_sphere', dx = 1e-7):
  
  mesh, nodeproxy, tetproxy, triproxy  = smeshio.importAbaqus('./mesh/tissue_sphere.inp', dx)
  smeshio.saveMesh('./mesh/tissue_sphere', mesh)
  
  tet_groups = tetproxy.blocksToGroups()
  
  domain_tets = tet_groups["Volume186"]
  c1_tets = tet_groups["Volume67"]
  c2_tets = tet_groups["Volume97"]
  c3_tets = tet_groups["Volume127"]
  c4_tets = tet_groups["Volume157"]

  memb1 = meshctrl.findOverlapTris(mesh, domain_tets, c1_tets)
  print(memb1)
  memb2 = meshctrl.findOverlapTris(mesh, domain_tets, c2_tets)
  memb3 = meshctrl.findOverlapTris(mesh, domain_tets, c3_tets)
  memb4 = meshctrl.findOverlapTris(mesh, domain_tets, c4_tets)
  
  vessel = sgeom.TmComp('tissue', mesh, domain_tets)
  vessel.addVolsys('tissue')
  cell1 = sgeom.TmComp('cell1', mesh, c1_tets)
  cell2 = sgeom.TmComp('cell2', mesh, c2_tets)
  cell3 = sgeom.TmComp('cell3', mesh, c3_tets)
  cell4 = sgeom.TmComp('cell4', mesh, c4_tets)


  cell1_memb = sgeom.TmPatch('memb1', mesh, memb1, icomp = cell1, ocomp = vessel)
  cell1_memb.addSurfsys('memb1')

  cell2_memb = sgeom.TmPatch('memb2', mesh, memb2, icomp = cell2, ocomp = vessel)
  cell2_memb.addSurfsys('memb2')

  cell3_memb = sgeom.TmPatch('memb3', mesh, memb3, icomp = cell3, ocomp = vessel)
  cell3_memb.addSurfsys('memb3')

  cell4_memb = sgeom.TmPatch('memb4', mesh, memb4, icomp = cell4, ocomp = vessel)
  cell4_memb.addSurfsys('memb4')

  return mesh, domain_tets, c1_tets, c2_tets, c3_tets, c4_tets