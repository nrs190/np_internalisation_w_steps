import steps.utilities.meshio as smeshio
import steps.model as smodel
import steps.geom as sgeom
import steps.rng as srng

def gen_model(k_diff = 1e-4, k_bind = 1e2, k_unbind = 1e-4, k_intern = 1e-5):
    
    mdl = smodel.Model()
    # NP
    NP = smodel.Spec('NP', mdl)     #free 
    NPi = smodel.Spec('NPi', mdl)   #internalized

    ############### cell state objects ###############
    # receptor state: 'naive' state (no bound NPs)
    R1 = smodel.Spec('R1', mdl)
    R2 = smodel.Spec('R2', mdl)
    R3 = smodel.Spec('R3', mdl)
    R4 = smodel.Spec('R4', mdl)

    # complexes state: NP bound to one cell 
    NPR1 = smodel.Spec('NPR1', mdl)
    # receptor state: NP bound to two cells
    NPR2 = smodel.Spec('NPR2', mdl)
    # receptor state: NP bound to three cells
    NPR3 = smodel.Spec('NPR3', mdl)
    # receptor state: NP bound to four cells
    NPR4 = smodel.Spec('NPR4', mdl)

    # Create separate volume systems for compartments A and B
    vsys_vessel = smodel.Volsys('vsys_vessel', mdl)
    vsys1 = smodel.Volsys('vsys1', mdl)
    vsys2 = smodel.Volsys('vsys2', mdl)
    vsys3 = smodel.Volsys('vsys3', mdl)
    vsys4 = smodel.Volsys('vsys4', mdl)

    # The 'forward' binding reactions:
    NP_R1_bind_NPR1 = smodel.Reac('NP_R1_bind_NPR1', vsys1,
                                lhs=[NP, R1], rhs=[NPR1], kcst=k_bind)
    NP_R2_bind_NPR2 = smodel.Reac('NP_R2_bind_NPR2', vsys2,
                                lhs=[NP, R2], rhs=[NPR2], kcst=k_bind)
    NP_R3_bind_NPR3 = smodel.Reac('NP_R3_bind_NPR3', vsys3,
                                lhs=[NP, R3], rhs=[NPR3], kcst=k_bind)
    NP_R4_bind_NPR4 = smodel.Reac('NP_R4_bind_NPR4', vsys4,
                                lhs=[NP, R4], rhs=[NPR4], kcst=k_bind)
    # Internalisation
    NPR1_internal = smodel.Reac('NPR1_internal', vsys1,
                               lhs=[NPR1], rhs=[R1, NPi], kcst=k_intern)
    NPR2_internal = smodel.Reac('NPR2_internal', vsys2,
                               lhs=[NPR2], rhs=[R2, NPi], kcst=k_intern)
    NPR3_internal = smodel.Reac('NPR3_internal', vsys3,
                               lhs=[NPR3], rhs=[R3, NPi], kcst=k_intern)
    NPR4_internal = smodel.Reac('NPR4_internal', vsys3,
                               lhs=[NPR4], rhs=[R4, NPi], kcst=k_intern)

    # The 'backward' unbinding reactions:
    NPR1_unbind_NP_R1 = smodel.Reac('NPR1_unbind_NP_R', vsys1,
                                lhs=[NPR1], rhs=[R1, NP], kcst=k_unbind)
    NPR2_unbind_NP_R2 = smodel.Reac('NPR2_unbind_NP_R', vsys2,
                                lhs=[NPR2], rhs=[R2, NP], kcst=k_unbind)
    NPR3_unbind_NP_R3 = smodel.Reac('NPR3_unbind_NP_R', vsys3,
                                lhs=[NPR3], rhs=[R3, NP], kcst=k_unbind)
    NPR4_unbind_NP_R4 = smodel.Reac('NPR4_unbind_NP_R', vsys4,
                                lhs=[NPR4], rhs=[R4, NP], kcst=k_unbind)

    # Diffusion <- this is the problem... , vsys1, vsys2, vsys3,vsys4]
    diff_NPv = smodel.Diff('diff_NPv', vsys_vessel, NP, dcst = k_diff)
    diff_NP1 = smodel.Diff('diff_NP1', vsys1, NP, dcst = k_diff)
    diff_NP2 = smodel.Diff('diff_NP2', vsys2, NP, dcst = k_diff)
    diff_NP3 = smodel.Diff('diff_NP3', vsys3, NP, dcst = k_diff)
    diff_NP4 = smodel.Diff('diff_NP4', vsys4, NP, dcst = k_diff)
    return mdl

def gen_geom(file_name = './mesh/cell_mesh', dx = 1e-7):
  mesh = smeshio.importAbaqus('./mesh/cell_mesh.inp', dx)
  smeshio.saveMesh('./mesh/cell_mesh', mesh[0])

  mesh = smeshio.loadMesh(file_name)[0]
  ntets = mesh.countTets()
  tets_vessel = []
  tets_cell1 = []
  tets_cell2 = [] 
  tets_cell3 = []
  tets_cell4 = [] 

  tris_vessel = set()
  tris_cell1 = set()
  tris_cell2 = set()
  tris_cell3 = set()
  tris_cell4 = set()

  l_max = mesh.getBoundMax()[1]
  l_min = mesh.getBoundMin()[1]
  print(l_max)
  l_mid = 10*dx
  l_0 = 0.0
  l_1 = 1*l_max/5
  l_2 = 2*l_max/5
  l_3 = 3*l_max/5
  l_4 = 4*l_max/5
  
  for t in range(ntets):
    # Fetch the z coordinate of the barycenter
    barycl = mesh.getTetBarycenter(t)[1]
    # Fetch the triangle indices of the tetrahedron, which is a tuple of length 4
    tris = mesh.getTetTriNeighb(t)
    if (l_0 <=  barycl <=  l_1):
      tets_vessel.append(t)
      tris_vessel.add(tris[0])
      tris_vessel.add(tris[1])
      tris_vessel.add(tris[2])
      tris_vessel.add(tris[3])
    elif (l_1 <= barycl <= l_2):
      tets_cell1.append(t)
      tris_cell1.add(tris[0])
      tris_cell1.add(tris[1])
      tris_cell1.add(tris[2])
      tris_cell1.add(tris[3])
    elif (l_2 <=  barycl <=  l_3 ):
      tets_cell2.append(t)
      tris_cell2.add(tris[0])
      tris_cell2.add(tris[1])
      tris_cell2.add(tris[2])
      tris_cell2.add(tris[3])
    elif (l_3 <=  barycl <=  l_4 ):
      tets_cell3.append(t)
      tris_cell3.add(tris[0])
      tris_cell3.add(tris[1])
      tris_cell3.add(tris[2])
      tris_cell3.add(tris[3])
    else:
      tets_cell4.append(t)
      tris_cell4.add(tris[0])
      tris_cell4.add(tris[1])
      tris_cell4.add(tris[2])
      tris_cell4.add(tris[3])
  
  vessel = sgeom.TmComp('vessel', mesh, tets_vessel)
  vessel.addVolsys('vsys_vessel')

  cell1 = sgeom.TmComp('cell1', mesh, tets_cell1)
  cell1.addVolsys('vsys1')
  tris_v1 = list(tris_cell1.intersection(tris_vessel))
#  tris_v1 = list(tris_v1)
  diffv1 = sgeom.DiffBoundary('diffv1', mesh, tris_v1)

  cell2 = sgeom.TmComp('cell2', mesh, tets_cell2)
  cell2.addVolsys('vsys2')
  tris_12 = list(tris_cell2.intersection(tris_cell1))
#  tris_12 = list(tris_12)
  diff12 = sgeom.DiffBoundary('diff12', mesh, tris_12)

  cell3 = sgeom.TmComp('cell3', mesh, tets_cell3)
  cell3.addVolsys('vsys3')
  tris_23 = list(tris_cell3.intersection(tris_cell2))
#  tris_23 = list(tris_23)
  diff23 = sgeom.DiffBoundary('diff23', mesh, tris_23)

  cell4 = sgeom.TmComp('cell4', mesh, tets_cell4)
  cell4.addVolsys('vsys4')
  tris_34 = list(tris_cell4.intersection(tris_cell3))
#  tris_34 = list(tris_34)
  diff34 = sgeom.DiffBoundary('diff34', mesh, tris_34)
  return mesh, tets_cell1, tets_cell2, tets_cell3, tets_cell4, tets_vessel
